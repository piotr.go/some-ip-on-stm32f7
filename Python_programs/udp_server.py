import socket
import time

src_ip = "192.168.0.10"
dst_ip = "192.168.0.20"
multicast_ip = "239.168.0.30"
unicast_port = 30491
multicast_port = 30490

sock_unicast = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
sock_unicast.bind((src_ip, unicast_port))

sock_multicast = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
sock_multicast.bind((src_ip, multicast_port))

i = 0
while True:
    if i % 2 == 0:
        sock_unicast.sendto(b"Hello STM32 via unicast", (dst_ip, unicast_port))
    else:
        sock_multicast.sendto(b"Hello STM32 via multicast", (multicast_ip, multicast_port))
    i += 1
    # data, addr = sock.recvfrom(1024)  # buffer size is 1024 bytes
    time.sleep(1)
