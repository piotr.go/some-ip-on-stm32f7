
from scapy.all import *
from scapy.contrib.automotive.someip import SOMEIP, SD, SDEntry_Service, SDOption_IP4_EndPoint
from scapy.layers.inet import IP, UDP

load_contrib('automotive.someip')

src_ip = "192.168.0.10"
dst_ip = "192.168.0.20"
multicast_ip = "239.168.0.30"
unicast_port = 30491
multicast_port = 30490

i_unicast = IP(dst=dst_ip, src=src_ip)
i_multicast = IP(dst=multicast_ip, src=src_ip)
u_unicast = UDP(dport=unicast_port, sport=unicast_port)
u_multicast = UDP(dport=multicast_port, sport=multicast_port)

# SOME/IP
sip = SOMEIP()
sip.srv_id = 1
sip.method_id = 2
# sip.len = 17
sip.client_id = 3
sip.session_id = 4
sip.proto_ver = 5
sip.iface_ver = 6
sip.msg_type = "REQUEST_NO_RETURN"
sip.retcode = "E_UNKNOWN_SERVICE"

sip.add_payload(Raw(b"Hello"))

# SOME/IP-SD
ea = SDEntry_Service()

ea.type = 0x01
ea.srv_id = 0x1234
ea.inst_id = 0x5678
ea.major_ver = 0x00
ea.ttl = 3

oa = SDOption_IP4_EndPoint()
oa.addr = "192.168.0.13"
oa.l4_proto = 0x11
oa.port = 30509

sd = SD()
sd.set_entryArray(ea)
sd.set_optionArray(oa)

# p_unicast = i_unicast/u_unicast/sip
p_unicast = sip
# p_multicast = i_multicast/u_multicast/sip/sd
p_multicast = sip/sd

sock_unicast = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
sock_unicast.bind((src_ip, unicast_port))

sock_multicast = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
sock_multicast.bind((src_ip, multicast_port))

i = 0
while True:
    if i % 2 == 0:
        # send(p_unicast)
        sock_unicast.sendto(raw(p_unicast), (dst_ip, unicast_port))
    else:
        # send(p_multicast)
        sock_multicast.sendto(raw(p_multicast), (multicast_ip, multicast_port))
    i += 1
    # data, addr = sock.recvfrom(1024)  # buffer size is 1024 bytes
    time.sleep(1)
