import socket

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
HOST = '192.168.0.10'
PORT = 1883        # Port to listen on (non-privileged ports are > 1023)

# with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
#     s.bind((HOST, PORT))
#     s.listen()
#     conn, addr = s.accept()
#     with conn:
#         print('Connected by', addr)
#         while True:
#             data = conn.recv(1024)
#             if not data:
#                 break
#             conn.sendall(data)

DST = '192.168.0.20'
DST_PORT = 62510
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.connect((DST, DST_PORT))
    s.sendall(b'Hello, world')
    data = s.recv(1024)
    # s.listen()
    # conn, addr = s.accept()
    # with conn:
    #     print('Connected by', addr)
    #     while True:
    #         data = conn.recv(1024)
    #         if not data:
    #             break
    #         conn.sendall(data)
