import time
from pyModbusTCP.server import ModbusServer, DataBank


def master_job(temp):
    DataBank.set_words(0, [temp])
    DataBank.set_words(1, [temp+1])


def main():
    server_host = '192.168.0.10'
    server_port = 502

    server = ModbusServer(server_host, server_port, no_block=True)
    server.start()

    temperature = 0
    while True:
        master_job(temperature)
        temperature += 1
        time.sleep(1)


if __name__ == "__main__":
    main()
