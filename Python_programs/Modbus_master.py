import time
from pyModbusTCP.client import ModbusClient


def slave_job(client):
    if not client.is_open():
        print("Unable to connect to server")

    if client.is_open():
        regs1 = client.read_holding_registers(1, 1)
        regs2 = client.read_holding_registers(0, 1)
        print(f'st={regs1}, y={regs2}')


def main():
    server_host = '192.168.0.21'
    server_port = 502

    client = ModbusClient()

    client.host(server_host)
    client.port(server_port)
    client.open()

    while True:
        slave_job(client)
        time.sleep(1)


if __name__ == "__main__":
    main()

