
payload_size = 240

payload = ''
byte_value = 0
byte_value_max = 0xff
for byte_idx in range(0, payload_size):
    payload += f", 0x{byte_value:02x}"
    byte_value += 1
    if byte_value > byte_value_max:
        byte_value = 0

print(payload)


# Modbus-TCP

payload_size = 125

payload = ''
byte_value = 0
byte_value_max = 0xff
for byte_idx in range(0, payload_size):
    payload += f", 0x{byte_value:02x}"
    byte_value += 1
    if byte_value > byte_value_max:
        byte_value = 0
    payload += f"{byte_value:02x}"
    byte_value += 1
    if byte_value > byte_value_max:
        byte_value = 0

print(payload)


