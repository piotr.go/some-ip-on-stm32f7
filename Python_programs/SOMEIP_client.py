"""
Program for simulate SOME/IP client device. It try to subscribe to
predefined service ID and Eventgroup ID. After proper subscription
it try to print every received data from server with some details
about received SOME/IP header.
"""

import asyncio
import ipaddress
import logging
import socket

import someip.header
from someip.config import Eventgroup, _T_SOCKNAME
from someip.sd import SOMEIPDatagramProtocol, ServiceDiscoveryProtocol

logging.getLogger("someip.sd").setLevel(logging.WARNING)
logging.getLogger("someip.sd.announce").setLevel(logging.WARNING)


def enhex(buf, sep=" "):
    return sep.join("%02x" % b for b in buf)


class EventGroupReceiver(SOMEIPDatagramProtocol):
    def __init__(self):
        super().__init__(logger="notification")
        self.prev_session_id = 0

    def message_received(
        self,
        someip_message: someip.header.SOMEIPHeader,
        addr: _T_SOCKNAME,
        multicast: bool,
    ) -> None:
        """
        called when a well-formed SOME/IP datagram was received
        """
        if someip_message.message_type != someip.header.SOMEIPMessageType.NOTIFICATION:
            self.log.warning("unexpected message type: %s", someip_message)
            return

        self.log.info(
            "payload=%s",
            enhex(someip_message.payload),
        )

        if (self.prev_session_id + 1) != someip_message.session_id:
            self.log.info(
                "session=0x%04x",
                someip_message.session_id,
            )

        self.prev_session_id = someip_message.session_id


async def run(
    local_addr, multicast_addr, local_port, service_id, instance_id, major_version, evgid
):
    trsp_u, trsp_m, protocol = await ServiceDiscoveryProtocol.create_endpoints(
        family=socket.AF_INET,
        local_addr=str(local_addr),
        multicast_addr=str(multicast_addr),
        port=30490,
    )

    evgrp_receiver, _ = await EventGroupReceiver.create_unicast_endpoint(
        local_addr=(str(local_addr), local_port)
    )
    sockname = evgrp_receiver.get_extra_info("sockname")

    try:
        protocol.start()
        protocol.discovery.find_subscribe_eventgroup(
            Eventgroup(
                service_id=service_id,
                instance_id=instance_id,
                major_version=major_version,
                eventgroup_id=evgid,
                sockname=sockname,
                protocol=someip.header.L4Protocols.UDP,
            )
        )
        while True:
            await asyncio.sleep(10)
    finally:
        protocol.stop()
        evgrp_receiver.close()
        trsp_u.close()
        trsp_m.close()


def auto_int(s):
    return int(s, 0)


def setup_log(fmt="", **kwargs):
    try:
        import coloredlogs  # type: ignore[import]
        coloredlogs.install(fmt="%(asctime)s,%(msecs)03d " + fmt, **kwargs)
    except ModuleNotFoundError:
        logging.basicConfig(format="%(asctime)s " + fmt, **kwargs)
        logging.info("install coloredlogs for colored logs :-)")


def main():
    setup_log(level=logging.DEBUG, fmt="%(levelname)-8s %(name)s: %(message)s")

    local_ip = "192.168.0.10"
    multicast_ip = "239.168.0.30"
    port = 38510
    service_id = 0x5258
    instance_id = 0x0
    major_version = 0x0
    eventgroup_id = 0x0001

    try:
        asyncio.get_event_loop().run_until_complete(
            run(
                local_ip,
                multicast_ip,
                port,
                service_id,
                instance_id,
                major_version,
                eventgroup_id,
            )
        )
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
