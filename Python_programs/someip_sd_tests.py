
import copy
from scapy.all import *
from scapy.contrib.automotive.someip import SOMEIP, SD, SDEntry_Service, SDEntry_EventGroup, SDOption_IP4_EndPoint

SOMEIP_PROTOCOL_TCP = 0x06
SOMEIP_PROTOCOL_UDP = 0x11

# SOME/IP message type
TYPE_REQUEST = "REQUEST"
TYPE_REQUEST_NO_RET = "REQUEST_NO_RETURN"
TYPE_NOTIFICATION = "NOTIFICATION"
TYPE_REQUEST_ACK = "REQUEST_ACK"
TYPE_REQUEST_NORET_ACK = "REQUEST_NO_RETURN_ACK"
TYPE_NOTIFICATION_ACK = "NOTIFICATION_ACK"
TYPE_RESPONSE = "RESPONSE"
TYPE_ERROR = "ERROR"
TYPE_RESPONSE_ACK = "RESPONSE_ACK"
TYPE_ERROR_ACK = "ERROR_ACK"
TYPE_TP_REQUEST = "TP_REQUEST"
TYPE_TP_REQUEST_NO_RET = "TP_REQUEST_NO_RETURN"
TYPE_TP_NOTIFICATION = "TP_NOTIFICATION"
TYPE_TP_RESPONSE = "TP_RESPONSE"
TYPE_TP_ERROR = "TP_ERROR"

RET_E_OK = "E_OK"
RET_E_NOT_OK = "E_NOT_OK"
RET_E_UNKNOWN_SERVICE = "E_UNKNOWN_SERVICE"
RET_E_UNKNOWN_METHOD = "E_UNKNOWN_METHOD"
RET_E_NOT_READY = "E_NOT_READY"
RET_E_NOT_REACHABLE = "E_NOT_REACHABLE"
RET_E_TIMEOUT = "E_TIMEOUT"
RET_E_WRONG_PROTOCOL_V = "E_WRONG_PROTOCOL_VERSION"
RET_E_WRONG_INTERFACE_V = "E_WRONG_INTERFACE_VERSION"
RET_E_MALFORMED_MSG = "E_MALFORMED_MESSAGE"
RET_E_WRONG_MESSAGE_TYPE = "E_WRONG_MESSAGE_TYPE"

SD_ET_FIND_SERVICE = 0x00
SD_ET_OFFER_SERVICE = 0x01
SD_ET_SUBSCRIBE = 0x06
SD_ET_SUBSCRIBE_ACK = 0x07


class Method:
    def __init__(self, identity, dst_ip=None, dst_port=None):
        self.id = identity
        self.dst_ip = dst_ip
        self.dst_port = dst_port


class Service:
    def __init__(self, identity, src_ip, src_port, multicast_ip=None):
        self.id = identity
        self.methods = []
        self.src_ip = src_ip
        self.src_port = src_port
        self.multicast_ip = multicast_ip

    def add_method(self, method_id, dst_ip=None, dst_port=None):
        method = Method(method_id, dst_ip, dst_port)
        self.methods.append(method)


class SomeipSdTest:
    device_type_client = "CLIENT"
    device_type_server = "SERVER"
    sd_port = 30490
    someip_protocol_version = 0x01
    someip_interface_version = 0x01
    sd_service_id = 0xFFFF
    sd_method_id = 0x8100
    g_session_id = 1
    ttl_find_service = 3
    ttl_offer_service = 3
    ttl_subscribe = 300
    ttl_subscribe_ack = 300

    def __init__(self, sd_src_ip, sd_dst_ip, multicast_ip):
        self.sd_src_ip = sd_src_ip
        self.sd_dst_ip = sd_dst_ip
        self.multicast_ip = multicast_ip

        self.src_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
        self.src_socket.bind((self.sd_src_ip, self.sd_port))

        # self.src_socket_multicast = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
        # self.src_socket_multicast.bind((self.multicast_ip, self.sd_port))

        sipsd = self._create_someip_hdr(self.sd_service_id, self.sd_method_id, TYPE_NOTIFICATION, RET_E_OK)
        self.sipsd = sipsd
        self.clientServices = []
        self.serverServices = []
        self.notification_session_id = 1

    def _create_someip_hdr(self, service_id, method_id, message_type, return_code):
        sip = SOMEIP()
        sip.srv_id = service_id
        sip.method_id = method_id
        sip.client_id = 0
        sip.session_id = 1
        sip.proto_ver = self.someip_protocol_version
        sip.iface_ver = self.someip_interface_version
        sip.msg_type = message_type
        sip.retcode = return_code
        return copy.deepcopy(sip)

    def _create_sd_hdr(self):
        sd = SD()
        sd.set_flag("REBOOT", 1)
        sd.set_flag("UNICAST", 1)
        return copy.deepcopy(sd)

    def _session_inc(self):
        self.g_session_id += 1
        if self.g_session_id == 65536:
            self.g_session_id = 1

    def add_service(self, device_type, service_id, src_ip, src_port, multicast_ip=None):
        service = Service(service_id, src_ip, src_port, multicast_ip)
        if device_type == self.device_type_client:
            self.clientServices.append(service)
        elif device_type == self.device_type_server:
            self.serverServices.append(service)
        else:
            return

    def add_method(self, device_type, service_id, method_id):
        if device_type == self.device_type_client:
            services = self.clientServices
        elif device_type == self.device_type_server:
            services = self.serverServices
        else:
            return

        for service in services:
            if service.id == service_id:
                service.add_method(method_id)

    def offer_service(self, debug_mode=False):
        entry_array = []
        option_array = []
        for service in self.serverServices:
            ea = SDEntry_Service()
            ea.type = SD_ET_OFFER_SERVICE
            ea.n_opt_1 = 1
            ea.srv_id = service.id
            ea.inst_id = 0
            ea.major_ver = 0
            ea.ttl = self.ttl_offer_service
            entry_array.append(ea)

            oa = SDOption_IP4_EndPoint()
            oa.addr = service.src_ip
            oa.l4_proto = SOMEIP_PROTOCOL_UDP
            oa.port = service.src_port

            if oa in option_array:
                pass
            else:
                option_array.append(oa)

        sd = self._create_sd_hdr()
        sd.set_entryArray(entry_array)
        sd.set_optionArray(option_array)

        sipsd = copy.deepcopy(self.sipsd)
        sipsd.session_id = self.g_session_id
        self._session_inc()

        sipsd = sipsd/sd
        self.src_socket.sendto(raw(sipsd), (self.multicast_ip, self.sd_port))

        if debug_mode:
            print(f"Send Offer service with session ID: {sipsd.session_id}")

    def subscribe_eventgroup(self, debug_mode=False):
        entry_array = []
        option_array = []
        for service in self.clientServices:
            for method in service.methods:
                ea = SDEntry_EventGroup()
                ea.type = SD_ET_SUBSCRIBE
                ea.n_opt_1 = 1
                ea.srv_id = service.id
                ea.inst_id = 0
                ea.ttl = self.ttl_subscribe
                ea.eventgroup_id = method.id
                entry_array.append(ea)

                oa = SDOption_IP4_EndPoint()
                oa.addr = service.src_ip
                oa.l4_proto = SOMEIP_PROTOCOL_UDP
                oa.port = service.src_port

                if oa in option_array:
                    pass
                else:
                    option_array.append(oa)

        sd = self._create_sd_hdr()
        sd.set_entryArray(entry_array)
        sd.set_optionArray(option_array)

        sipsd = copy.deepcopy(self.sipsd)
        sipsd.session_id = self.g_session_id
        self._session_inc()

        sipsd = sipsd/sd
        self.src_socket.sendto(raw(sipsd), (self.sd_dst_ip, self.sd_port))

        if debug_mode:
            print(f"Send Subscribe eventgroup service with session ID: {sipsd.session_id}")

    def send_notification(self, ip_address, port, service_id, method_id, debug_mode=False):
        sip = self._create_someip_hdr(service_id, method_id, TYPE_NOTIFICATION, RET_E_OK)
        sip.session_id = self.notification_session_id
        self.notification_session_id += 1
        sip.add_payload(Raw(b"Hello"))
        self.src_socket.sendto(raw(sip), (ip_address, port))
        if debug_mode:
            print(f"Send Notification with session ID: {sip.session_id}")

def main():
    pc_ip = "192.168.0.10"
    stm32_ip = "192.168.0.20"
    multicast_ip = "239.168.0.30"

    client_local_port = 30410
    client_local_port_stm32 = 30420
    client_service_id_1 = 0xB0A7
    client_service_id_2 = 0x259
    client_method_id_1 = 0x1
    client_method_id_2 = 0x2

    src_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
    src_socket.bind((pc_ip, client_local_port))

    server_local_port = 30510
    server_service_id_1 = 0x5258
    server_service_id_2 = 0x5259
    server_method_id_1 = 0x5147
    server_method_id_2 = 0x5369

    src_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
    src_socket.bind((pc_ip, server_local_port))

    tester = SomeipSdTest(pc_ip, stm32_ip, multicast_ip)
    tester.add_service(SomeipSdTest.device_type_server, client_service_id_1, pc_ip, client_local_port)
    tester.add_method(SomeipSdTest.device_type_server, client_service_id_1, client_method_id_1)
    tester.add_method(SomeipSdTest.device_type_server, client_service_id_1, client_method_id_2)
    # tester.add_service(SomeipSdTest.device_type_server, client_service_id_2, pc_ip, client_local_port)
    # tester.add_method(SomeipSdTest.device_type_server, client_service_id_2, client_method_id_1)

    tester.add_service(SomeipSdTest.device_type_client, server_service_id_1, pc_ip, server_local_port)
    tester.add_method(SomeipSdTest.device_type_client, server_service_id_1, server_method_id_1)
    tester.add_service(SomeipSdTest.device_type_client, server_service_id_2, pc_ip, server_local_port)
    tester.add_method(SomeipSdTest.device_type_client, server_service_id_2, server_method_id_1)

    while True:
        tester.offer_service(debug_mode=True)
        # tester.subscribe_eventgroup(debug_mode=True)

        tester.send_notification(stm32_ip, client_local_port_stm32, client_service_id_1, client_method_id_1,
                                 debug_mode=True)
        time.sleep(1)
        tester.send_notification(stm32_ip, client_local_port_stm32, client_service_id_1, client_method_id_1,
                                 debug_mode=True)
        time.sleep(1)


if __name__ == "__main__":
    main()
