/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "lwip.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <string.h>
#include "lwip/ip4_addr.h"
#include "lwip/tcp.h"
#include "lightmodbus/lightmodbus.h"
#include "lightmodbus/master.h"
#include "lightmodbus/slave.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/**
 * DEVICE_TYPE flag value decide about what kind of device role this application
 * will be realized. With DT_SLAVE flag application wait for receive function from
 * Master. With DT_MASTER flag application try send function to slave.
 */
#define DT_SLAVE 0
#define DT_MASTER 1
#define DEVICE_TYPE DT_MASTER
/* During change DEVICE_TYPE for DT_SLAVE, please change IP and MAC addresses
 * in lwip.c (line 73) and enternetif.c (line 216). */


/**
 * TEST_TYPE flag value decide about what kind of test will be performed.
 * TT_FUNCTIONALITY - basic test shows simple transfer data from server to client
 * TT_LATENCY - test to measure latency from send to receive Modbus TCP message
 * TT_BANDWIDTH - test to measure bandwidth of Modbus TCP protocol by try to send as
 * many frame as it is possible
 */
#define TT_LATENCY 0
#define TT_BANDWIDTH 1
#define TEST_TYPE TT_BANDWIDTH

#define UART_MSG_MAX_SIZE 128

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

TIM_HandleTypeDef htim10;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

#if TEST_TYPE == TT_LATENCY
#define MODBUS_REGS_MAX_SIZE 16
uint8_t coils[2];
uint16_t regs[MODBUS_REGS_MAX_SIZE] = {
		0x0001, 0x0203, 0x0405, 0x0607, 0x0809, 0x0a0b, 0x0c0d, 0x0e0f, 0x1011, 0x1213, 0x1415, 0x1617, 0x1819, 0x1a1b, 0x1c1d, 0x1e1f
};

#define TEST_NO_MAX 1000
u16_t testResults[TEST_NO_MAX];
u32_t testNo = 0;
#endif

#if TEST_TYPE == TT_BANDWIDTH

#define MODBUS_REGS_MAX_SIZE 122
uint8_t coils[2];
uint16_t regs[MODBUS_REGS_MAX_SIZE] = {
		0x0001, 0x0203, 0x0405, 0x0607, 0x0809, 0x0a0b, 0x0c0d, 0x0e0f, 0x1011, 0x1213, 0x1415, 0x1617, 0x1819, 0x1a1b, 0x1c1d, 0x1e1f, 0x2021, 0x2223, 0x2425, 0x2627, 0x2829, 0x2a2b, 0x2c2d, 0x2e2f, 0x3031, 0x3233, 0x3435, 0x3637, 0x3839, 0x3a3b, 0x3c3d, 0x3e3f, 0x4041, 0x4243, 0x4445, 0x4647, 0x4849, 0x4a4b, 0x4c4d, 0x4e4f, 0x5051, 0x5253, 0x5455, 0x5657, 0x5859, 0x5a5b, 0x5c5d, 0x5e5f, 0x6061, 0x6263, 0x6465, 0x6667, 0x6869, 0x6a6b, 0x6c6d, 0x6e6f, 0x7071, 0x7273, 0x7475, 0x7677, 0x7879, 0x7a7b, 0x7c7d, 0x7e7f, 0x8081, 0x8283, 0x8485, 0x8687, 0x8889, 0x8a8b, 0x8c8d, 0x8e8f, 0x9091, 0x9293, 0x9495, 0x9697, 0x9899, 0x9a9b, 0x9c9d, 0x9e9f, 0xa0a1, 0xa2a3, 0xa4a5, 0xa6a7, 0xa8a9, 0xaaab, 0xacad, 0xaeaf, 0xb0b1, 0xb2b3, 0xb4b5, 0xb6b7, 0xb8b9, 0xbabb, 0xbcbd, 0xbebf, 0xc0c1, 0xc2c3, 0xc4c5, 0xc6c7, 0xc8c9, 0xcacb, 0xcccd, 0xcecf, 0xd0d1, 0xd2d3, 0xd4d5, 0xd6d7, 0xd8d9, 0xdadb, 0xdcdd, 0xdedf, 0xe0e1, 0xe2e3, 0xe4e5, 0xe6e7, 0xe8e9, 0xeaeb, 0xeced, 0xeeef, 0xf0f1, 0xf2f3
};

struct testResult {
	u32_t frame_all_counter;
	u32_t frame_malformed_counter;
	u32_t frame_skipped_counter;
};

u32_t testPeriodS = 100;  // [s]
#define TEST_NO_MAX 100
struct testResult testResults[TEST_NO_MAX];
u32_t testNo = 0;
u8_t slave_ready_flag = 1;
u8_t master_ready_flag = 0;

#endif

char uartMsg[UART_MSG_MAX_SIZE];
u32_t uartMsgSize = 0;

#if DEVICE_TYPE == DT_SLAVE

struct tcp_pcb *tcp_server_pcb;

/* ECHO protocol states */
enum tcp_server_states
{
  ES_NONE = 0,
  ES_ACCEPTED,
  ES_RECEIVED,
  ES_CLOSING
};

/* structure for maintaing connection infos to be passed as argument
   to LwIP callbacks*/
struct tcp_server_struct
{
  u8_t state;             /* current connection state */
  u8_t retries;
  struct tcp_pcb *pcb;    /* pointer on the current tcp_pcb */
  struct pbuf *p;         /* pointer on the received/to be transmitted pbuf */
};

#elif DEVICE_TYPE == DT_MASTER

u16_t gModbusTcpMsgCounter = 1;
struct client *gEs = NULL;

struct tcp_pcb *client_pcb;

/* ECHO protocol states */
enum client_states
{
  ES_NOT_CONNECTED = 0,
  ES_CONNECTED,
  ES_RECEIVED,
  ES_CLOSING,
};

/* structure to be passed as argument to the tcp callbacks */
struct client
{
  enum client_states state; /* connection status */
  struct tcp_pcb *pcb;          /* pointer on the current tcp_pcb */
  struct pbuf *p_tx;            /* pointer on pbuf to be transmitted */
};

#endif

PACK_STRUCT_BEGIN
struct modbusTcpHdr_t {
	PACK_STRUCT_FIELD(u16_t id);
	PACK_STRUCT_FIELD(u16_t protocol);
	PACK_STRUCT_FIELD(u16_t length);
} 	PACK_STRUCT_STRUCT;
PACK_STRUCT_END

// getters
#define modbusTcpHdr_id(modbusTcpHdr) 			ntohs((modbusTcpHdr)->id)
#define modbusTcpHdr_protocol(modbusTcpHdr) 	ntohs((modbusTcpHdr)->protocol)
#define modbusTcpHdr_length(modbusTcpHdr) 		ntohs((modbusTcpHdr)->length)

// setters
#define modbusTcpHdr_set_id(modbusTcpHdr, x) 		((modbusTcpHdr)->id = htons(x))
#define modbusTcpHdr_set_protocol(modbusTcpHdr, x)	((modbusTcpHdr)->protocol = htons(x))
#define modbusTcpHdr_set_length(modbusTcpHdr, x) 	((modbusTcpHdr)->length = htons(x))

#define MODBUS_TCP_HDR_LEN (sizeof(struct modbusTcpHdr_t))  // [byte]

#if DEVICE_TYPE == DT_SLAVE

//Configuration structures
ModbusSlave sstatus;

#elif DEVICE_TYPE == DT_MASTER

//Configuration structures
ModbusMaster mstatus;

#endif

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM10_Init(void);
/* USER CODE BEGIN PFP */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void printTestResults();

#if DEVICE_TYPE == DT_SLAVE

void tcp_server_init(const ip_addr_t *ipaddr, u16_t port);
static err_t tcp_server_accept(void *arg, struct tcp_pcb *newpcb, err_t err);
static err_t tcp_server_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
static void tcp_server_error(void *arg, err_t err);
static err_t tcp_server_poll(void *arg, struct tcp_pcb *tpcb);
static err_t tcp_server_sent(void *arg, struct tcp_pcb *tpcb, u16_t len);
static void tcp_server_send(struct tcp_pcb *tpcb, struct tcp_server_struct *es);
static void tcp_server_connection_close(struct tcp_pcb *tpcb, struct tcp_server_struct *es);

#elif DEVICE_TYPE == DT_MASTER

static err_t tcp_client_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
static void tcp_client_connection_close(struct tcp_pcb *tpcb, struct client * es);
static err_t tcp_client_poll(void *arg, struct tcp_pcb *tpcb);
static err_t tcp_client_sent(void *arg, struct tcp_pcb *tpcb, u16_t len);
static void tcp_client_send(struct tcp_pcb *tpcb, struct client * es);
static err_t tcp_client_connected(void *arg, struct tcp_pcb *tpcb, err_t err);
void tcp_client_connect(const ip_addr_t *ipaddr, u16_t port);

#endif

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_LWIP_Init();
  MX_USART1_UART_Init();
  MX_TIM10_Init();
  /* USER CODE BEGIN 2 */

  HAL_Delay(1000);

  	u16_t modbusPort = 502;
    struct ip4_addr tcpSlaveAddr;
  	IP4_ADDR(&tcpSlaveAddr, 192, 168, 0, 21);
//  	IP4_ADDR(&tcpSlaveAddr, 192, 168, 0, 10);

#if DEVICE_TYPE == DT_SLAVE
  	tcp_server_init(&tcpSlaveAddr, modbusPort);

  	//Init slave (input registers and discrete inputs work just the same)
	sstatus.address = 1;
	sstatus.registers = regs;
	sstatus.registerCount = MODBUS_REGS_MAX_SIZE;
	sstatus.coils = coils;
	sstatus.coilCount = 16;
	modbusSlaveInit( &sstatus );

#elif DEVICE_TYPE == DT_MASTER
//    struct ip4_addr tcpMasterAddr;
//  	IP4_ADDR(&tcpMasterAddr, 192, 168, 0, 20);
	tcp_client_connect(&tcpSlaveAddr, modbusPort);
	modbusMasterInit( &mstatus );

#endif

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

	uint32_t startupPeriod = 3000;
	uint32_t startupTimeout = HAL_GetTick() + startupPeriod;
	uint32_t taskPeriod = 1000;
	uint32_t taskTimeout = HAL_GetTick() + taskPeriod;
#if TEST_TYPE == TT_LATENCY
	uint32_t testPeriod = 50;
	uint32_t testTimeout = HAL_GetTick() + startupTimeout + testPeriod + 3000;
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_SLAVE)
	uint32_t testPeriod = testPeriodS*1000;
	uint32_t testTimeout = 0;
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_MASTER)
	uint32_t testPeriod = TEST_NO_MAX*(testPeriodS+1)*1000;  // TEST_NO_MAX * 11 second
	uint32_t testTimeout = 0;
#endif

	u8_t modbusStartedFlag = 0;

	while (1)
	{
		MX_LWIP_Process();

		if((HAL_GetTick() > startupTimeout) && (!modbusStartedFlag)) {
			modbusStartedFlag = 1;
#if (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_MASTER)
		    testTimeout = HAL_GetTick() + testPeriod;
#endif
		}

#if (TEST_TYPE == TT_LATENCY) && (DEVICE_TYPE == DT_SLAVE)
		if (testNo == TEST_NO_MAX) {
			printTestResults();
			testNo++;
		}
#elif (TEST_TYPE == TT_LATENCY) && (DEVICE_TYPE == DT_MASTER)
		if ((slave_ready_flag) && (modbusStartedFlag) && (testNo < TEST_NO_MAX)) {
			slave_ready_flag = 0;

			HAL_GPIO_TogglePin(GPIO_OUTPUT_GPIO_Port, GPIO_OUTPUT_Pin);

			tcp_client_send(client_pcb, gEs);

//			uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "%ld\n\r", testNo);
//			HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);

			regs[0] += 1;
			testNo++;

			testTimeout = HAL_GetTick() + testPeriod;
		}
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_SLAVE)
	    if (master_ready_flag && (testTimeout == 0)) {
	    	testTimeout = HAL_GetTick() + testPeriod;
	    }

	    if(master_ready_flag && (HAL_GetTick() > testTimeout) && (testNo < TEST_NO_MAX)) {
	    	testNo++;
	    	testTimeout = HAL_GetTick() + testPeriod;
	    }

	    if(testNo == TEST_NO_MAX) {
			printTestResults();
			testNo++;
	    }
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_MASTER)
		if((HAL_GetTick() < testTimeout) && (modbusStartedFlag) && (slave_ready_flag)) {
			slave_ready_flag = 0;
			tcp_client_send(client_pcb, gEs);
			regs[0] += 1;
		}
#endif

		if (HAL_GetTick() > taskTimeout) {
			HAL_GPIO_TogglePin(LED_blue_GPIO_Port, LED_blue_Pin);

			taskTimeout = HAL_GetTick() + taskPeriod;
		}

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 108;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM10 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM10_Init(void)
{

  /* USER CODE BEGIN TIM10_Init 0 */

  /* USER CODE END TIM10_Init 0 */

  /* USER CODE BEGIN TIM10_Init 1 */

  /* USER CODE END TIM10_Init 1 */
  htim10.Instance = TIM10;
  htim10.Init.Prescaler = 0;
  htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim10.Init.Period = 65535;
  htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim10.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim10) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM10_Init 2 */

  /* USER CODE END TIM10_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIO_OUTPUT_GPIO_Port, GPIO_OUTPUT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_blue_GPIO_Port, LED_blue_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : GPIO_OUTPUT_Pin */
  GPIO_InitStruct.Pin = GPIO_OUTPUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIO_OUTPUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_INPUT_Pin */
  GPIO_InitStruct.Pin = GPIO_INPUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIO_INPUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LED_blue_Pin */
  GPIO_InitStruct.Pin = LED_blue_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_blue_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */

void printTestResults()
{
#if (TEST_TYPE == TT_LATENCY) && (DEVICE_TYPE == DT_SLAVE)

	uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "Test results:\n\r");
	HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);

	u32_t idx;
	for(idx = 0; idx < TEST_NO_MAX; idx++) {
		uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "%ld %d\n\r", idx, testResults[idx]);
		HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);
	}
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_SLAVE)
	uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "Test results:\n\r");
	HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);
	uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "no all malformed skipped:\n\r");
	HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);

	u32_t idx;
	for(idx = 0; idx < TEST_NO_MAX; idx++) {
		uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "%ld %ld %ld %ld\n\r", idx, testResults[idx].frame_all_counter, testResults[idx].frame_malformed_counter, testResults[idx].frame_skipped_counter);
		HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);
	}
#endif
}

#if DEVICE_TYPE == DT_SLAVE

struct pbuf * getModbusTcpResponse(struct pbuf *request)
{
	struct modbusTcpHdr_t *modbusTcpHdrRequest;
	u8_t *modbusRtuRequestPtr;
	u16_t modbusTcpRequestLen, modbusRtuRequestLen, modbusTcpResponseLen;
	struct pbuf *response;

	modbusTcpHdrRequest = request->payload;
	modbusTcpRequestLen = modbusTcpHdr_length(modbusTcpHdrRequest);
	modbusRtuRequestLen = modbusTcpRequestLen + 2;
	modbusRtuRequestPtr = (uint8_t *) calloc( modbusRtuRequestLen, sizeof( uint8_t ) );
	memcpy(modbusRtuRequestPtr, (u8_t *)(request->payload) + MODBUS_TCP_HDR_LEN, modbusTcpRequestLen);
	sstatus.request.frame = modbusRtuRequestPtr;
	sstatus.request.length = modbusRtuRequestLen;

#if (TEST_TYPE == TT_LATENCY) && (DEVICE_TYPE == DT_SLAVE)
	HAL_TIM_Base_Stop(&htim10);  // Stop time measurement on subscriber side

	int idx;
	for (idx = 0; (idx < sstatus.request.length) && (idx < MODBUS_REGS_MAX_SIZE); idx++) {
		if (ntohs(*((uint16_t *)(sstatus.request.frame + (2 * idx) + 7))) != (regs[idx])) {
			if (testNo < TEST_NO_MAX) {
				testResults[testNo] = 0xFFFF;
			}
		}
	}

	modbusParseRequest( &sstatus );
	free(modbusRtuRequestPtr);

	if (testNo < TEST_NO_MAX) {
		if (testResults[testNo] != 0xFFFF) {
			testResults[testNo] = __HAL_TIM_GetCounter(&htim10);
		}
	}
	__HAL_TIM_SetCounter(&htim10, 0);

	regs[0] += 1;
	testNo++;
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_SLAVE)
	master_ready_flag = 1;

	testResults[testNo].frame_all_counter++;

	u16_t value;
	value = ntohs(*((uint16_t *)(sstatus.request.frame + 7)));
	if ((testResults[testNo].frame_all_counter > 1) && (value != regs[0])) {
		testResults[testNo].frame_skipped_counter += ntohs(*((uint16_t *)(sstatus.request.frame + 7))) - regs[0];
	}

	int idx;
	u8_t frame_malformed_flag = 0;
	for (idx = 1; (idx < (sstatus.request.length - 7)) && (idx < MODBUS_REGS_MAX_SIZE); idx++) {
		value = ntohs(*((uint16_t *)(sstatus.request.frame + (2 * idx) + 7)));
		if (value != (regs[idx])) {
			frame_malformed_flag = 1;
		}
	}

	modbusParseRequest( &sstatus );
	free(modbusRtuRequestPtr);

	if (frame_malformed_flag) {
		testResults[testNo].frame_malformed_counter++;
	}

	value = ntohs(*((uint16_t *)(sstatus.request.frame + 7)));
	regs[0] = value + 1;
#endif

	modbusTcpResponseLen = MODBUS_TCP_HDR_LEN + sstatus.response.length - 2;
	response = pbuf_alloc(PBUF_TRANSPORT, modbusTcpResponseLen, PBUF_POOL);  // 6 bytes for ModbusTCP header, 2 bytes for CRC
	MEMCPY((u8_t *)response->payload, (u8_t *)modbusTcpHdrRequest, MODBUS_TCP_HDR_LEN);
	modbusTcpHdrRequest = response->payload;
	modbusTcpHdr_set_length(modbusTcpHdrRequest, modbusTcpResponseLen - MODBUS_TCP_HDR_LEN);
	MEMCPY(((u8_t *)response->payload) + MODBUS_TCP_HDR_LEN, sstatus.response.frame, modbusTcpResponseLen - MODBUS_TCP_HDR_LEN);

	return response;
}


/**
  * @brief  Initializes the tcp echo server
  * @param  None
  * @retval None
  */
void tcp_server_init(const ip_addr_t *ipaddr, u16_t port)
{
  /* create new tcp pcb */
  tcp_server_pcb = tcp_new();

  if (tcp_server_pcb != NULL)
  {
    err_t err;

    /* bind echo_pcb to port 7 (ECHO protocol) */
    err = tcp_bind(tcp_server_pcb, ipaddr, port);

    if (err == ERR_OK)
    {
      /* start tcp listening for echo_pcb */
      tcp_server_pcb = tcp_listen(tcp_server_pcb);

      /* initialize LwIP tcp_accept callback function */
      tcp_accept(tcp_server_pcb, tcp_server_accept);
    }
    else
    {
      /* deallocate the pcb */
      memp_free(MEMP_TCP_PCB, tcp_server_pcb);
    }
  }
}

/**
  * @brief  This function is the implementation of tcp_accept LwIP callback
  * @param  arg: not used
  * @param  newpcb: pointer on tcp_pcb struct for the newly created tcp connection
  * @param  err: not used
  * @retval err_t: error status
  */
static err_t tcp_server_accept(void *arg, struct tcp_pcb *newpcb, err_t err)
{
  err_t ret_err;
  struct tcp_server_struct *es;

  LWIP_UNUSED_ARG(arg);
  LWIP_UNUSED_ARG(err);

  /* set priority for the newly accepted tcp connection newpcb */
  tcp_setprio(newpcb, TCP_PRIO_MIN);

  /* allocate structure es to maintain tcp connection informations */
  es = (struct tcp_server_struct *)mem_malloc(sizeof(struct tcp_server_struct));
  if (es != NULL)
  {
    es->state = ES_ACCEPTED;
    es->pcb = newpcb;
    es->retries = 0;
    es->p = NULL;

    /* pass newly allocated es structure as argument to newpcb */
    tcp_arg(newpcb, es);

    /* initialize lwip tcp_recv callback function for newpcb  */
    tcp_recv(newpcb, tcp_server_recv);

    /* initialize lwip tcp_err callback function for newpcb  */
    tcp_err(newpcb, tcp_server_error);

    /* initialize lwip tcp_poll callback function for newpcb */
    tcp_poll(newpcb, tcp_server_poll, 0);

    ret_err = ERR_OK;
  }
  else
  {
    /*  close tcp connection */
    tcp_server_connection_close(newpcb, es);
    /* return memory error */
    ret_err = ERR_MEM;
  }
  return ret_err;
}


/**
  * @brief  This function is the implementation for tcp_recv LwIP callback
  * @param  arg: pointer on a argument for the tcp_pcb connection
  * @param  tpcb: pointer on the tcp_pcb connection
  * @param  pbuf: pointer on the received pbuf
  * @param  err: error information regarding the reveived pbuf
  * @retval err_t: error code
  */
static err_t tcp_server_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err)
{
  struct tcp_server_struct *es;
  err_t ret_err;

  LWIP_ASSERT("arg != NULL",arg != NULL);

  es = (struct tcp_server_struct *)arg;

  /* if we receive an empty tcp frame from client => close connection */
  if (p == NULL)
  {
    /* remote host closed connection */
    es->state = ES_CLOSING;
    if(es->p == NULL)
    {
       /* we're done sending, close connection */
       tcp_server_connection_close(tpcb, es);
    }
    else
    {
      /* we're not done yet */
      /* acknowledge received packet */
      tcp_sent(tpcb, tcp_server_sent);

      /* send remaining data*/
      tcp_server_send(tpcb, es);
    }
    ret_err = ERR_OK;
  }
  /* else : a non empty frame was received from client but for some reason err != ERR_OK */
  else if(err != ERR_OK)
  {
    /* free received pbuf*/
    if (p != NULL)
    {
      es->p = NULL;
      pbuf_free(p);
    }
    ret_err = err;
  }
  else if(es->state == ES_ACCEPTED)
  {
    /* first data chunk in p->payload */
    es->state = ES_RECEIVED;

    /* store reference to incoming pbuf (chain) */
    es->p = p;

    /* initialize LwIP tcp_sent callback function */
    tcp_sent(tpcb, tcp_server_sent);

    /* send back the received data (echo) */
    tcp_server_send(tpcb, es);

    ret_err = ERR_OK;
  }
  else if (es->state == ES_RECEIVED)
  {
    /* more data received from client and previous data has been already sent*/
    if(es->p == NULL)
    {
      es->p = p;

      /* send back received data */
      tcp_server_send(tpcb, es);
    }
    else
    {
      struct pbuf *ptr;

      /* chain pbufs to the end of what we recv'ed previously  */
      ptr = es->p;
      pbuf_chain(ptr,p);
    }
    ret_err = ERR_OK;
  }
  else if(es->state == ES_CLOSING)
  {
    /* odd case, remote side closing twice, trash data */
    tcp_recved(tpcb, p->tot_len);
    es->p = NULL;
    pbuf_free(p);
    ret_err = ERR_OK;
  }
  else
  {
    /* unkown es->state, trash data  */
    tcp_recved(tpcb, p->tot_len);
    es->p = NULL;
    pbuf_free(p);
    ret_err = ERR_OK;
  }
  return ret_err;
}

/**
  * @brief  This function implements the tcp_err callback function (called
  *         when a fatal tcp_connection error occurs.
  * @param  arg: pointer on argument parameter
  * @param  err: not used
  * @retval None
  */
static void tcp_server_error(void *arg, err_t err)
{
  struct tcp_server_struct *es;

  LWIP_UNUSED_ARG(err);

  es = (struct tcp_server_struct *)arg;
  if (es != NULL)
  {
    /*  free es structure */
    mem_free(es);
  }
}

/**
  * @brief  This function implements the tcp_poll LwIP callback function
  * @param  arg: pointer on argument passed to callback
  * @param  tpcb: pointer on the tcp_pcb for the current tcp connection
  * @retval err_t: error code
  */
static err_t tcp_server_poll(void *arg, struct tcp_pcb *tpcb)
{
  err_t ret_err;
  struct tcp_server_struct *es;

  es = (struct tcp_server_struct *)arg;
  if (es != NULL)
  {
    if (es->p != NULL)
    {
      tcp_sent(tpcb, tcp_server_sent);
      /* there is a remaining pbuf (chain) , try to send data */
      tcp_server_send(tpcb, es);
    }
    else
    {
      /* no remaining pbuf (chain)  */
      if(es->state == ES_CLOSING)
      {
        /*  close tcp connection */
        tcp_server_connection_close(tpcb, es);
      }
    }
    ret_err = ERR_OK;
  }
  else
  {
    /* nothing to be done */
    tcp_abort(tpcb);
    ret_err = ERR_ABRT;
  }
  return ret_err;
}

/**
  * @brief  This function implements the tcp_sent LwIP callback (called when ACK
  *         is received from remote host for sent data)
  * @param  None
  * @retval None
  */
static err_t tcp_server_sent(void *arg, struct tcp_pcb *tpcb, u16_t len)
{
  struct tcp_server_struct *es;

  LWIP_UNUSED_ARG(len);

  es = (struct tcp_server_struct *)arg;
  es->retries = 0;

  if(es->p != NULL)
  {
    /* still got pbufs to send */
    tcp_sent(tpcb, tcp_server_sent);
    tcp_server_send(tpcb, es);
  }
  else
  {
    /* if no more data to send and client closed connection*/
    if(es->state == ES_CLOSING)
      tcp_server_connection_close(tpcb, es);
  }
  return ERR_OK;
}


/**
  * @brief  This function is used to send data for tcp connection
  * @param  tpcb: pointer on the tcp_pcb connection
  * @param  es: pointer on echo_state structure
  * @retval None
  */
static void tcp_server_send(struct tcp_pcb *tpcb, struct tcp_server_struct *es)
{
  struct pbuf *ptr;
  err_t wr_err = ERR_OK;

  while ((wr_err == ERR_OK) &&
         (es->p != NULL) &&
         (es->p->len <= tcp_sndbuf(tpcb)))
  {

    /* get pointer on pbuf from es structure */
    ptr = es->p;

    /* enqueue data for transmission */
    struct pbuf *response = getModbusTcpResponse(ptr);
    wr_err = tcp_write(tpcb, response->payload, response->len, 1);
	tcp_output(tpcb);
    pbuf_free(response);

    if (wr_err == ERR_OK)
    {
      u16_t plen;
      u8_t freed;

      plen = ptr->len;

      /* continue with next pbuf in chain (if any) */
      es->p = ptr->next;

      if(es->p != NULL)
      {
        /* increment reference count for es->p */
        pbuf_ref(es->p);
      }

     /* chop first pbuf from chain */
      do
      {
        /* try hard to free pbuf */
        freed = pbuf_free(ptr);
      }
      while(freed == 0);
     /* we can read more data now */
     tcp_recved(tpcb, plen);
   }
   else if(wr_err == ERR_MEM)
   {
      /* we are low on memory, try later / harder, defer to poll */
     es->p = ptr;
   }
   else
   {
     /* other problem ?? */
   }
  }
}

/**
  * @brief  This functions closes the tcp connection
  * @param  tcp_pcb: pointer on the tcp connection
  * @param  es: pointer on echo_state structure
  * @retval None
  */
static void tcp_server_connection_close(struct tcp_pcb *tpcb, struct tcp_server_struct *es)
{

  /* remove all callbacks */
  tcp_arg(tpcb, NULL);
  tcp_sent(tpcb, NULL);
  tcp_recv(tpcb, NULL);
  tcp_err(tpcb, NULL);
  tcp_poll(tpcb, NULL, 0);

  /* delete es structure */
  if (es != NULL)
  {
    mem_free(es);
  }

  /* close tcp connection */
  tcp_close(tpcb);
}

#elif DEVICE_TYPE == DT_MASTER

struct pbuf * setModbusTcpRequest(void)
{
	struct modbusTcpHdr_t *modbusTcpHdrRequest;
	u16_t modbusTcpRequestLen;
	struct pbuf *request;

	u16_t slave_address, startRegister, count;
	slave_address = 1;
	startRegister = 0;
	count = MODBUS_REGS_MAX_SIZE;
	modbusBuildRequest16( &mstatus, slave_address, startRegister, count, regs );

	modbusTcpRequestLen = MODBUS_TCP_HDR_LEN + mstatus.request.length - 2;  // 6 bytes for ModbusTCP header, 2 bytes for CRC
	request = pbuf_alloc(PBUF_TRANSPORT, modbusTcpRequestLen, PBUF_POOL);
	modbusTcpHdrRequest = request->payload;

	modbusTcpHdr_set_id(modbusTcpHdrRequest, gModbusTcpMsgCounter++);
	modbusTcpHdr_set_protocol(modbusTcpHdrRequest, 0);
	modbusTcpHdr_set_length(modbusTcpHdrRequest, mstatus.request.length - 2);
	MEMCPY(((u8_t *)request->payload) + MODBUS_TCP_HDR_LEN, mstatus.request.frame, modbusTcpRequestLen - MODBUS_TCP_HDR_LEN);

	return request;
}


void parseModbusTcpResponse(struct pbuf *response)
{
	struct modbusTcpHdr_t *modbusTcpHdrRequest;
	u8_t *modbusRtuRequestPtr;
	u16_t modbusTcpRequestLen, modbusRtuRequestLen;

	modbusTcpHdrRequest = response->payload;
	modbusTcpRequestLen = modbusTcpHdr_length(modbusTcpHdrRequest);
	modbusRtuRequestLen = modbusTcpRequestLen + 2;
	modbusRtuRequestPtr = (uint8_t *) calloc( modbusRtuRequestLen, sizeof( uint8_t ) );
	memcpy(modbusRtuRequestPtr, (u8_t *)(response->payload) + MODBUS_TCP_HDR_LEN, modbusTcpRequestLen);
	mstatus.response.frame = modbusRtuRequestPtr;
	mstatus.response.length = modbusRtuRequestLen;

	modbusParseResponse( &mstatus );

	slave_ready_flag = 1;

	free(modbusRtuRequestPtr);
}


/**
  * @brief  Connects to the TCP echo server
  * @param  None
  * @retval None
  */
void tcp_client_connect(const ip_addr_t *ipaddr, u16_t port)
{
  /* create new tcp pcb */
  client_pcb = tcp_new();

  if (client_pcb != NULL)
  {
    /* connect to destination address/port */
    tcp_connect(client_pcb, ipaddr, port, tcp_client_connected);
  }
  else
  {
    /* deallocate the pcb */
    memp_free(MEMP_TCP_PCB, client_pcb);
  }
}

/**
  * @brief Function called when TCP connection established
  * @param tpcb: pointer on the connection contol block
  * @param err: when connection correctly established err should be ERR_OK
  * @retval err_t: returned error
  */
static err_t tcp_client_connected(void *arg, struct tcp_pcb *tpcb, err_t err)
{
//  struct client *es = NULL;

  if (err == ERR_OK)
  {
    /* allocate structure es to maintain tcp connection informations */
    gEs = (struct client *)mem_malloc(sizeof(struct client));

    if (gEs != NULL)
    {
      gEs->state = ES_CONNECTED;
      gEs->pcb = tpcb;

      //sprintf((char*)data, "sending tcp client message %d", (int)message_count);

      /* allocate pbuf */
      //gEs->p_tx = pbuf_alloc(PBUF_TRANSPORT, strlen((char*)data) , PBUF_POOL);

      if (gEs->p_tx)
      {
        /* copy data to pbuf */
        //pbuf_take(gEs->p_tx, (char*)data, strlen((char*)data));

        /* pass newly allocated es structure as argument to tpcb */
        tcp_arg(tpcb, gEs);

        /* initialize LwIP tcp_recv callback function */
        tcp_recv(tpcb, tcp_client_recv);

        /* initialize LwIP tcp_sent callback function */
        tcp_sent(tpcb, tcp_client_sent);

        /* initialize LwIP tcp_poll callback function */
        tcp_poll(tpcb, tcp_client_poll, 1);

        /* send data */
        //tcp_client_send(tpcb,gEs);

        return ERR_OK;
      }
    }
    else
    {
      /* close connection */
      tcp_client_connection_close(tpcb, gEs);

      /* return memory allocation error */
      return ERR_MEM;
    }
  }
  else
  {
    /* close connection */
    tcp_client_connection_close(tpcb, gEs);
  }
  return err;
}

/**
  * @brief tcp_receiv callback
  * @param arg: argument to be passed to receive callback
  * @param tpcb: tcp connection control block
  * @param err: receive error code
  * @retval err_t: retuned error
  */
static err_t tcp_client_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err)
{
  struct client *es;
  err_t ret_err;

  LWIP_ASSERT("arg != NULL",arg != NULL);

  es = (struct client *)arg;

  /* if we receive an empty tcp frame from server => close connection */
  if (p == NULL)
  {
    /* remote host closed connection */
    es->state = ES_CLOSING;
    if(es->p_tx == NULL)
    {
       /* we're done sending, close connection */
       tcp_client_connection_close(tpcb, es);
    }
    else
    {
      /* send remaining data*/
      tcp_client_send(tpcb, es);
    }
    ret_err = ERR_OK;
  }
  /* else : a non empty frame was received from echo server but for some reason err != ERR_OK */
  else if(err != ERR_OK)
  {
    /* free received pbuf*/
    if (p != NULL)
    {
      pbuf_free(p);
    }
    ret_err = err;
  }
  else if(es->state == ES_CONNECTED)
  {
    /* increment message count */
//    message_count++;

	parseModbusTcpResponse(p);

    /* Acknowledge data reception */
    tcp_recved(tpcb, p->tot_len);

    pbuf_free(p);
//    tcp_client_connection_close(tpcb, es);
    ret_err = ERR_OK;
  }

  /* data received when connection already closed */
  else
  {
    /* Acknowledge data reception */
    tcp_recved(tpcb, p->tot_len);

    /* free pbuf and do nothing */
    pbuf_free(p);
    ret_err = ERR_OK;
  }
  return ret_err;
}

/**
  * @brief function used to send data
  * @param  tpcb: tcp control block
  * @param  es: pointer on structure of type client containing info on data
  *             to be sent
  * @retval None
  */
static void tcp_client_send(struct tcp_pcb *tpcb, struct client * es)
{
	struct pbuf *request = setModbusTcpRequest();

	tcp_write(tpcb, request->payload, request->len, 1);
	tcp_output(tpcb);
	pbuf_free(request);

//  struct pbuf *ptr;
//  err_t wr_err = ERR_OK;
//
//  while ((wr_err == ERR_OK) &&
//         (es->p_tx != NULL) &&
//         (es->p_tx->len <= tcp_sndbuf(tpcb)))
//  {
//
//    /* get pointer on pbuf from es structure */
//    ptr = es->p_tx;
//
//    /* enqueue data for transmission */
//
//    struct pbuf *request = setModbusTcpRequest();
//    wr_err = tcp_write(tpcb, request->payload, request->len, 1);
//
//    if (wr_err == ERR_OK)
//    {
//      /* continue with next pbuf in chain (if any) */
//      es->p_tx = ptr->next;
//
//      if(es->p_tx != NULL)
//      {
//        /* increment reference count for es->p */
//        pbuf_ref(es->p_tx);
//      }
//
//      /* free pbuf: will free pbufs up to es->p (because es->p has a reference count > 0) */
//      pbuf_free(ptr);
//   }
//   else if(wr_err == ERR_MEM)
//   {
//      /* we are low on memory, try later, defer to poll */
//     es->p_tx = ptr;
//   }
//   else
//   {
//     /* other problem ?? */
//   }
//  }
}

/**
  * @brief  This function implements the tcp_poll callback function
  * @param  arg: pointer on argument passed to callback
  * @param  tpcb: tcp connection control block
  * @retval err_t: error code
  */
static err_t tcp_client_poll(void *arg, struct tcp_pcb *tpcb)
{
  err_t ret_err;
  struct client *es;

  es = (struct client*)arg;
  if (es != NULL)
  {
    if (es->p_tx != NULL)
    {
      /* there is a remaining pbuf (chain) , try to send data */
//      tcp_client_send(tpcb, es);
    }
    else
    {
      /* no remaining pbuf (chain)  */
      if(es->state == ES_CLOSING)
      {
        /* close tcp connection */
        tcp_client_connection_close(tpcb, es);
      }
    }
    ret_err = ERR_OK;
  }
  else
  {
    /* nothing to be done */
    tcp_abort(tpcb);
    ret_err = ERR_ABRT;
  }
  return ret_err;
}

/**
  * @brief  This function implements the tcp_sent LwIP callback (called when ACK
  *         is received from remote host for sent data)
  * @param  arg: pointer on argument passed to callback
  * @param  tcp_pcb: tcp connection control block
  * @param  len: length of data sent
  * @retval err_t: returned error code
  */
static err_t tcp_client_sent(void *arg, struct tcp_pcb *tpcb, u16_t len)
{
  struct client *es;

  LWIP_UNUSED_ARG(len);

  es = (struct client *)arg;

  if(es->p_tx != NULL)
  {
    /* still got pbufs to send */
    //tcp_client_send(tpcb, es);
  }

  return ERR_OK;
}

/**
  * @brief This function is used to close the tcp connection with server
  * @param tpcb: tcp connection control block
  * @param es: pointer on client structure
  * @retval None
  */
static void tcp_client_connection_close(struct tcp_pcb *tpcb, struct client * es )
{
  /* remove callbacks */
  tcp_recv(tpcb, NULL);
  tcp_sent(tpcb, NULL);
  tcp_poll(tpcb, NULL,0);

  if (es != NULL)
  {
    mem_free(es);
  }

  /* close tcp connection */
  tcp_close(tpcb);
}

#endif

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
#if (TEST_TYPE == TT_LATENCY) && (DEVICE_TYPE == DT_SLAVE)
	if(GPIO_Pin == GPIO_INPUT_Pin) {
		HAL_TIM_Base_Start(&htim10);  // Start time measurement on subscriber side
	}
#endif
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
