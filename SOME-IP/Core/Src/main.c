/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "lwip.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <string.h>
#include "lwip/ip4_addr.h"
#include "lwip/udp.h"
#include "lwip/igmp.h"
#include "someip.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */


/**
 * DEVICE_TYPE flag value decide about what kind of device role this application
 * will be realized. With DT_CLIENT flag application try to subscribe to specified
 * services and methods. With DT_SERVER flag application try send cyclically
 * offer service message and react for receive subscribe message.
 */
#define DT_CLIENT 0
#define DT_SERVER 1
#define DEVICE_TYPE DT_SERVER
/* During change DEVICE_TYPE for DT_CLIENT, please change IP and MAC addresses
 * in lwip.c (line 73) and enternetif.c (line 216). */


/**
 * TEST_TYPE flag value decide about what kind of test will be performed.
 * TT_FUNCTIONALITY - basic test shows simple transfer data from server to client
 * TT_LATENCY - test to measure latency from send to receive SOME/IP message
 * TT_BANDWIDTH - test to measure bandwidth of SOME/IP protocol by try to send as
 * many frame as it is possible
 */
#define TT_FUNCTIONALITY 0
#define TT_LATENCY 1
#define TT_BANDWIDTH 2
#define TEST_TYPE TT_BANDWIDTH

#define UART_MSG_MAX_SIZE 128

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

TIM_HandleTypeDef htim10;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

#if TEST_TYPE == TT_FUNCTIONALITY
#define SOMEIP_MSG_MAX_SIZE 32
char someipMsg[SOMEIP_MSG_MAX_SIZE];
u32_t someipMsgSize = 0;
u8_t msgCounter = 0;

#elif TEST_TYPE == TT_LATENCY
#define SOMEIP_MSG_MAX_SIZE 32
char someipMsg[SOMEIP_MSG_MAX_SIZE] = {
		0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f
};

#define TEST_NO_MAX 1000
u16_t testResults[TEST_NO_MAX];
u32_t testNo = 0;
#endif

#if TEST_TYPE == TT_BANDWIDTH
#define SOMEIP_MSG_MAX_SIZE 1390
char someipMsg[SOMEIP_MSG_MAX_SIZE] = {
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d
};

struct testResult {
	u32_t frame_all_counter;
	u32_t frame_malformed_counter;
	u32_t frame_skipped_counter;
};

u32_t testPeriodS = 100;  // [s]
#define TEST_NO_MAX 100
struct testResult testResults[TEST_NO_MAX];
u32_t testNo = 0;
u8_t client_ready_flag = 1;
u8_t server_ready_flag = 0;

#endif

char uartMsg[UART_MSG_MAX_SIZE];
u32_t uartMsgSize = 0;

u16_t serviceId_1 = 0x5258;
u16_t methodId_1 = 0x0001;
#if (TEST_TYPE == TT_FUNCTIONALITY) && (DEVICE_TYPE == DT_CLIENT)
u16_t serviceId_2 = 0x5259;
u16_t methodId_2 = 0x0002;
#elif (TEST_TYPE == TT_FUNCTIONALITY) && (DEVICE_TYPE == DT_SERVER)
u16_t serviceId_2 = 0x5257;
u16_t methodId_2 = 0x0002;
#endif

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM10_Init(void);
/* USER CODE BEGIN PFP */

err_t someipStart();
void printTestResults();
void someipClientCallback(struct pbuf *p, const ip_addr_t *addr, u16_t port, u16_t serviceId, u16_t methodId);
void someipServerCallback(struct pbuf *p, const ip_addr_t *addr, u16_t port, u16_t serviceId, u16_t methodId);
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_LWIP_Init();
  MX_USART1_UART_Init();
  MX_TIM10_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

	err_t status;

	uint32_t startupPeriod = 5000;
	uint32_t startupTimeout = HAL_GetTick() + startupPeriod;
	uint32_t taskPeriod = 1000;
	uint32_t taskTimeout = HAL_GetTick() + taskPeriod;
#if TEST_TYPE == TT_FUNCTIONALITY
	uint32_t testPeriod = 1000;
	uint32_t testTimeout = HAL_GetTick() + testPeriod;
#elif TEST_TYPE == TT_LATENCY
	uint32_t testPeriod = 10;
	uint32_t testTimeout = HAL_GetTick() + startupTimeout + testPeriod + 3000;
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_CLIENT)
	uint32_t testPeriod = testPeriodS*1000;
	uint32_t testTimeout = 0;
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_SERVER)
	uint32_t testPeriod = TEST_NO_MAX*(testPeriodS+1)*1000;  // TEST_NO_MAX * 11 second
	uint32_t testTimeout = 0;
#endif

	struct pbuf *pb;

	u8_t someipStartedFlag = 0;

    status = someipStart();

	while (1)
	{
		MX_LWIP_Process();

		status = someip_task();
	    if (status != ERR_OK) {
	    	return status;
	    }

		if((HAL_GetTick() > startupTimeout) && (!someipStartedFlag)) {
		    if (status != ERR_OK) {
			    return status;
		    }
		    someipStartedFlag = 1;
#if (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_SERVER)
		    testTimeout = HAL_GetTick() + testPeriod;
#endif
		}

		if (someipStartedFlag) {
#if (TEST_TYPE == TT_FUNCTIONALITY) && (DEVICE_TYPE == DT_SERVER)
			if(HAL_GetTick() > testTimeout) {
				pb = pbuf_alloc(PBUF_TRANSPORT, 1, PBUF_POOL);
				if (pb != NULL) {
					pb->payload = someipMsg;
					someip_sendServerMsg(serviceId_1, methodId_1, SOMEIP_MT_NOTIFICATION, pb);
					pbuf_free(pb);
					someipMsg[0]++;
				}
				testTimeout = HAL_GetTick() + testPeriod;
			}
#elif (TEST_TYPE == TT_LATENCY) && (DEVICE_TYPE == DT_CLIENT)
		    if (testNo == TEST_NO_MAX) {
				printTestResults();
				testNo++;
			}
#elif (TEST_TYPE == TT_LATENCY) && (DEVICE_TYPE == DT_SERVER)
		    if((HAL_GetTick() > testTimeout) && (testNo < TEST_NO_MAX)) {
		    	HAL_GPIO_TogglePin(GPIO_OUTPUT_GPIO_Port, GPIO_OUTPUT_Pin);  // Start test on client side

		    	pb = pbuf_alloc(PBUF_TRANSPORT, SOMEIP_MSG_MAX_SIZE, PBUF_POOL);
		    	if (pb != NULL) {
		    		strcpy(pb->payload, someipMsg);
		    		someip_sendServerMsg(serviceId_1, methodId_1, SOMEIP_MT_NOTIFICATION, pb);
		    		pbuf_free(pb);
			    	someipMsg[0] += 1;
			    	testNo++;
		    	}

		    	testTimeout = HAL_GetTick() + testPeriod;
			}
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_CLIENT)
		    if (server_ready_flag && (testTimeout == 0)) {
		    	testTimeout = HAL_GetTick() + testPeriod;
		    }

		    if(server_ready_flag && (HAL_GetTick() > testTimeout) && (testNo < TEST_NO_MAX)) {
		    	testNo++;
		    	testTimeout = HAL_GetTick() + testPeriod;
		    }

		    if(testNo == TEST_NO_MAX) {
				printTestResults();
				testNo++;
		    }
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_SERVER)
		    if((HAL_GetTick() < testTimeout) && (client_ready_flag)) {
		    	pb = pbuf_alloc(PBUF_TRANSPORT, SOMEIP_MSG_MAX_SIZE, PBUF_POOL);
				if (pb != NULL) {
					MEMCPY((u8_t *)pb->payload, (u8_t *)someipMsg, SOMEIP_MSG_MAX_SIZE);
					someip_sendServerMsg(serviceId_1, methodId_1, SOMEIP_MT_NOTIFICATION, pb);
					pbuf_free(pb);
					someipMsg[0] += 1;
					client_ready_flag = 0;
				}
		    }
#endif
		}

		if(HAL_GetTick() > taskTimeout) {
			HAL_GPIO_TogglePin(LED_blue_GPIO_Port, LED_blue_Pin);
			taskTimeout = HAL_GetTick() + taskPeriod;
		}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 108;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM10 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM10_Init(void)
{

  /* USER CODE BEGIN TIM10_Init 0 */

  /* USER CODE END TIM10_Init 0 */

  /* USER CODE BEGIN TIM10_Init 1 */

  /* USER CODE END TIM10_Init 1 */
  htim10.Instance = TIM10;
  htim10.Init.Prescaler = 0;
  htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim10.Init.Period = 65535;
  htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim10.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim10) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM10_Init 2 */

  /* USER CODE END TIM10_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIO_OUTPUT_GPIO_Port, GPIO_OUTPUT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_blue_GPIO_Port, LED_blue_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : GPIO_OUTPUT_Pin */
  GPIO_InitStruct.Pin = GPIO_OUTPUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIO_OUTPUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : GPIO_INPUT_Pin */
  GPIO_InitStruct.Pin = GPIO_INPUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIO_INPUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LED_blue_Pin */
  GPIO_InitStruct.Pin = LED_blue_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_blue_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */


/**
 * Function to start SOME/IP protocol library. According to device type services are registered
 * to server or client. Next one methods were created and connect to service.
 * @return ERR_OK if start SOME/IP was performed properly, otherwise err_t error code
 */
err_t someipStart()
{
	struct ip4_addr udpSrcAddr;
	struct ip4_addr udpMulticastAddr;

	struct someip_service_t *service;

	err_t status;

#if (TEST_TYPE == TT_FUNCTIONALITY) && (DEVICE_TYPE == DT_CLIENT)
	IP4_ADDR(&udpSrcAddr, 192, 168, 0, 20);
//	IP4_ADDR(&udpSrcAddr, 192, 168, 0, 21);
#elif (TEST_TYPE == TT_FUNCTIONALITY) && (DEVICE_TYPE == DT_SERVER)
	IP4_ADDR(&udpSrcAddr, 192, 168, 0, 20);
#elif (DEVICE_TYPE == DT_SERVER)
	IP4_ADDR(&udpSrcAddr, 192, 168, 0, 20);
#elif (DEVICE_TYPE == DT_CLIENT)
	IP4_ADDR(&udpSrcAddr, 192, 168, 0, 21);
#endif

	IP4_ADDR(&udpMulticastAddr, 239, 168, 0, 30);

	// SOME/IP

	u32_t cyclicFindOfferDelay = 2;  // [s]
	status = someip_init(&udpSrcAddr, &udpMulticastAddr, cyclicFindOfferDelay);
	if (status != ERR_OK) {
		return status;
	}


#if (DEVICE_TYPE == DT_CLIENT) && (TEST_TYPE == TT_FUNCTIONALITY)
	u16_t clientSrcPort_1 = 30420;
	u16_t clientSrcPort_2 = 30421;

	status = someip_addClientService(&service, serviceId_1, &udpSrcAddr, clientSrcPort_1, SOMEIP_PROTOCOL_UDP);
	if (status != ERR_OK) {
		return status;
	}

	status = someip_addMethod(service, methodId_1, someipClientCallback);
	if (status != ERR_OK) {
		return status;
	}

	status = someip_addClientService(&service, serviceId_2, &udpSrcAddr, clientSrcPort_2, SOMEIP_PROTOCOL_UDP);
	if (status != ERR_OK) {
		return status;
	}

	status = someip_addMethod(service, methodId_1, someipClientCallback);
	if (status != ERR_OK) {
		return status;
	}

	status = someip_addMethod(service, methodId_2, someipClientCallback);
	if (status != ERR_OK) {
		return status;
	}
#elif (DEVICE_TYPE == DT_SERVER) && (TEST_TYPE == TT_FUNCTIONALITY)
	u16_t serverSrcPort_1 = 30520;
	u16_t serverSrcPort_2 = 30521;

	status = someip_addServerService(&service, serviceId_1, &udpSrcAddr, serverSrcPort_1, SOMEIP_PROTOCOL_UDP);
	if (status != ERR_OK) {
		return status;
	}

	status = someip_addMethod(service, methodId_1, someipClientCallback);
	if (status != ERR_OK) {
		return status;
	}

	status = someip_addServerService(&service, serviceId_2, &udpSrcAddr, serverSrcPort_2, SOMEIP_PROTOCOL_UDP);
	if (status != ERR_OK) {
		return status;
	}

	status = someip_addMethod(service, methodId_1, someipClientCallback);
	if (status != ERR_OK) {
		return status;
	}

	status = someip_addMethod(service, methodId_2, someipClientCallback);
	if (status != ERR_OK) {
		return status;
	}
#endif


#if (DEVICE_TYPE == DT_CLIENT) && ((TEST_TYPE == TT_LATENCY) || (TEST_TYPE == TT_BANDWIDTH))
	u16_t clientSrcPort_1 = 30420;

	status = someip_addClientService(&service, serviceId_1, &udpSrcAddr, clientSrcPort_1, SOMEIP_PROTOCOL_UDP);
	if (status != ERR_OK) {
		return status;
	}

	status = someip_addMethod(service, methodId_1, someipClientCallback);
	if (status != ERR_OK) {
		return status;
	}
#endif

#if (DEVICE_TYPE == DT_SERVER) && ((TEST_TYPE == TT_LATENCY) || (TEST_TYPE == TT_BANDWIDTH))
	u16_t serverSrcPort_1 = 30520;

	status = someip_addServerService(&service, serviceId_1, &udpSrcAddr, serverSrcPort_1, SOMEIP_PROTOCOL_UDP);
	if (status != ERR_OK) {
		return status;
	}

	status = someip_addMethod(service, methodId_1, someipServerCallback);
	if (status != ERR_OK) {
		return status;
	}
#endif

	return ERR_OK;
}


/**
 * Print result of performed tests
 */
void printTestResults()
{
#if (TEST_TYPE == TT_LATENCY) && (DEVICE_TYPE == DT_CLIENT)

	uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "Test results:\n\r");
	HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);

	u32_t idx;
	for(idx = 0; idx < TEST_NO_MAX; idx++) {
		uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "%ld %d\n\r", idx, testResults[idx]);
		HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);
	}
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_CLIENT)
	uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "Test results:\n\r");
	HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);
	uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "no all malformed skipped:\n\r");
	HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);

	u32_t idx;
	for(idx = 0; idx < TEST_NO_MAX; idx++) {
		uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "%ld %ld %ld %ld\n\r", idx, testResults[idx].frame_all_counter, testResults[idx].frame_malformed_counter, testResults[idx].frame_skipped_counter);
		HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);
	}
#endif
}


/**
 * Callback for received SOME/IP frame on Client side
 */
void someipClientCallback(struct pbuf *p, const ip_addr_t *addr, u16_t port, u16_t serviceId, u16_t methodId)
{
#if (TEST_TYPE == TT_FUNCTIONALITY) && (DEVICE_TYPE == DT_CLIENT)
	uartMsgSize = snprintf(uartMsg, UART_MSG_MAX_SIZE, "Message received: %d\n\r", *((uint8_t*)(p->payload)));
	HAL_UART_Transmit(&huart1, (u8_t *)uartMsg, uartMsgSize, 1000);
#elif (TEST_TYPE == TT_LATENCY) && (DEVICE_TYPE == DT_CLIENT)
	HAL_TIM_Base_Stop(&htim10);  // Stop time measurement on client side

	int idx;
	for (idx = 0; (idx < p->len) && (idx < SOMEIP_MSG_MAX_SIZE); idx++) {
		if ((*(((u8_t *)(p->payload)) + idx)) != someipMsg[idx]) {
			testResults[testNo] = 0xFFFF;
		}
	}

	if (testResults[testNo] != 0xFFFF) {
		testResults[testNo] = __HAL_TIM_GetCounter(&htim10);
	}
	__HAL_TIM_SetCounter(&htim10, 0);

	someipMsg[0] += 1;
	testNo++;
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_CLIENT)
	server_ready_flag = 1;

	testResults[testNo].frame_all_counter++;

	if ((testResults[testNo].frame_all_counter > 1) && ((*((u8_t *)(p->payload))) != someipMsg[0])) {  // do not check for first frame
		testResults[testNo].frame_skipped_counter += *((u8_t *)(p->payload)) - someipMsg[0];
	}

	int idx;
	u8_t frame_malformed_flag = 0;
	u8_t value;
	for (idx = 1; (idx < p->len) && (idx < SOMEIP_MSG_MAX_SIZE); idx++) {
		value = *(((u8_t *)(p->payload)) + idx);
		if (value != someipMsg[idx]) {
			frame_malformed_flag = 1;
		}
	}

	if (frame_malformed_flag) {
		testResults[testNo].frame_malformed_counter++;
	}

	someipMsg[0] = *((u8_t *)(p->payload)) + 1;

	HAL_GPIO_TogglePin(GPIO_OUTPUT_GPIO_Port, GPIO_OUTPUT_Pin);  // inform server that client is ready for frame

#endif
	pbuf_free(p);
}

/**
 * Callback for received SOME/IP frame on Server side
 */
void someipServerCallback(struct pbuf *p, const ip_addr_t *addr, u16_t port, u16_t serviceId, u16_t methodId)
{
	pbuf_free(p);
}


/**
 * Callback for GPIO interruption
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
#if (TEST_TYPE == TT_LATENCY) && (DEVICE_TYPE == DT_CLIENT)
	if(GPIO_Pin == GPIO_INPUT_Pin) {
		HAL_TIM_Base_Start(&htim10);  // Start time measurement on client side
	}
#elif (TEST_TYPE == TT_BANDWIDTH) && (DEVICE_TYPE == DT_SERVER)
	if(GPIO_Pin == GPIO_INPUT_Pin) {
		client_ready_flag = 1;
	}
#endif
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
