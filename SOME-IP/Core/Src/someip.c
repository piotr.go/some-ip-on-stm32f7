/*
 * someip.c
 *
 *  Created on: 19.03.2021
 *      Author: Piotr Golyzniak
 */


#include "someip.h"
#include "lwip/udp.h"
#include "lwip/igmp.h"

#include <string.h>

/* BEGIN global variables */

/* First element of singly linked list of services  */
struct someip_service_t *someipSdService = NULL;
struct someip_service_t *clientServices = NULL;
struct someip_service_t *serverServices = NULL;

/* Basic structures for SOME/IP and SD messages  */
struct someipHdr_t someipHdrBasic;
struct someipHdr_t someipSdHdrBasic;
struct sdHdr_t sdHdrBasic;
struct sdEntry_t entryBasic;
struct sdOptionIpv4_t optionIpv4Basic;

/* Interface to contain information about multicast address and port  */
struct someip_deviceInterface_t *gMulticastDeviceInterface;

/* Variables for define information about time tasks  */
u32_t gInitialDelayMs;  // [ms]
u32_t gInitialDelayDoneFlag;
u32_t gPreviousTimeMs;  // [ms]
u32_t gFindRepetitions;
u32_t gBasicFindDelayMs;  // [ms]
u32_t gCyclicFindDelayMs;  // [ms]
u32_t gCurrentFindDelayMs;  // [ms]
u32_t gCyclicOfferDelayMs;  // [ms]
u32_t gCurrentOfferDelayMs;  // [ms]

/* Variable to contain information about error code which
 * was detected during analyze received message */
u8_t gRceiveMsgErrorCode;

/* Callback to handle received SD message by user  */
someipCallback_t gUserSdCallbackFcn;

/* Buffer for storage frame send during finding or offering service */
struct pbuf *gPbFindService;
struct pbuf *gPbOfferService;

/* Flag for indicate reboot detection */
u8_t gRebootFlag = 1;

/* END global variables */


/* BEGIN function declarations */

static err_t addDeviceInterface(struct someip_deviceInterface_t **firstDeviceInterface, struct someip_deviceInterface_t **newDeviceInterface, ip4_addr_t *ip4Addr, u16_t port, u8_t protocol, s32_t ttl, enum someip_dir_t direction);

/* END function declarations */


/* BEGIN function definitions */

/**
 * Update length field in SOME/IP header based on current buffer length
 * @param pbSomeipMsg Pointer to buffer which contains SOME/IP frame
 * @return ERR_OK if update was performed properly, otherwise err_t error code
 */
static err_t someipUpdateLength(struct pbuf *pbSomeipMsg)
{
	struct someipHdr_t *someipSdHdr;

	if (pbSomeipMsg == NULL) {
		return ERR_ARG;
	}

	someipSdHdr = pbSomeipMsg->payload;
	someipHdr_set_length(someipSdHdr, pbSomeipMsg->len - SOMEIP_MSGID_LEN_LENGTH);

	return ERR_OK;
}


/**
 * Add IPv4 option to Options Array in created SOME/IP-SD message. If option already exist do not add new option.
 * @param optionPtr Pointer to buffer with current Options array
 * @param deviceInterface Pointer to device interface which contains information about address and
 * port which will be passed to created option
 * @param optionIpv4Cnt Pointer to amount of options in current Options array
 * @param optionIdxLookFor Pointer to index of added (or found) option in Options array
 * @param optionType Type of option defined in someip.h file as #define SD_OT_
 * @return ERR_OK if option was added or found properly, otherwise err_t error code
 */
static err_t addOptionIpv4(u8_t *optionPtr, struct someip_deviceInterface_t *deviceInterface, u16_t *optionIpv4Cnt, u8_t *optionIdxLookFor, u8_t optionType)
{
	u8_t optionIdx;
	struct sdOptionIpv4_t *optionIpv4;

	if ((optionPtr == NULL) || (deviceInterface == NULL) || (optionIpv4Cnt == NULL) || (optionIdxLookFor == NULL)) {
		return ERR_ARG;
	}

	for (optionIdx = 0; optionIdx < *optionIpv4Cnt; ++optionIdx) {
		optionIpv4 = (struct sdOptionIpv4_t *)optionPtr;
		optionPtr += sizeof(struct sdOptionIpv4_t);

		if ((sdOptionIpv4_address(optionIpv4) == ip4_addr_get_u32(&(deviceInterface->ip4Addr)))
				&& (sdOptionIpv4_port(optionIpv4) == deviceInterface->port)
				&& (sdOptionIpv4_protocol(optionIpv4) == deviceInterface->protocol)) {
			break;
		}
	}

	if (optionIdx == *optionIpv4Cnt) {  // option not found
		optionIpv4 = (struct sdOptionIpv4_t *)optionPtr;
		MEMCPY((u8_t *)optionIpv4, (u8_t *)(&optionIpv4Basic), sizeof(struct sdOptionIpv4_t));
		sdOptionIpv4_set_type(optionIpv4, optionType);
		sdOptionIpv4_set_address(optionIpv4, ip4_addr_get_u32(&(deviceInterface->ip4Addr)));
		sdOptionIpv4_set_port(optionIpv4, deviceInterface->port);
		sdOptionIpv4_set_protocol(optionIpv4, deviceInterface->protocol);
		++(*optionIpv4Cnt);
	}

	*optionIdxLookFor = optionIdx;

	return ERR_OK;
}


/**
 * Initialize SD message by set SOME/IP-SD message header
 * @param currentBufferPtr Pointer to pointer to buffer with basic SOME/IP-SD message
 * @param sessionId Actual value of session ID for sending message
 * @param rebootFlag Actual value of reboot flag for sending message
 * @return ERR_OK if message was initialized properly, otherwise err_t error code
 */
static err_t sdMessageInit(u8_t **currentBufferPtr, u16_t sessionId, u8_t rebootFlag)
{
	struct someipHdr_t *someipHdr;
	struct sdHdr_t *sdHdr;

	if (currentBufferPtr == NULL) {
		return ERR_ARG;
	}

	someipHdr = (struct someipHdr_t *)(*currentBufferPtr);
	MEMCPY((u8_t *)someipHdr, (u8_t *)(&someipSdHdrBasic), sizeof(struct someipHdr_t));
	*currentBufferPtr = *currentBufferPtr + sizeof(struct someipHdr_t);
	someipHdr_set_sessionId(someipHdr, sessionId);

	sdHdr = (struct sdHdr_t *)(*currentBufferPtr);
	MEMCPY((u8_t *)sdHdr, (u8_t *)(&sdHdrBasic), sizeof(struct sdHdr_t));
	*currentBufferPtr = *currentBufferPtr + sizeof(struct sdHdr_t);
	sdHdr_set_reboot(sdHdr, rebootFlag);

	return ERR_OK;
}


/**
 * Send SD message with Subscribe eventgroup and Subscribe eventgorup acknowledge entries
 * @param entrySubscribeCnt amount of subscribe entries to be send
 * @param entrySubscribeCnt amount of subscribe ack entries to be send
 * @param deviceInterface pointer to device interface which will be used to get information
 * about destination IPv4 address and port number
 * @return ERR_OK if message was send properly, otherwise err_t error code
 */
static err_t sendSubscribeAndSubscribeAck(u16_t entrySubscribeCnt, u16_t entrySubscribeAckCnt, struct someip_deviceInterface_t *deviceInterface)
{
	struct pbuf *pbSdMsg, *pbOption;
	u8_t *currentSdMsgPtr;
	struct someip_service_t *service;
	struct someip_method_t *method;
	u32_t entryArrayLength;
	struct sdEntry_t *entry;
	u16_t optionIpv4Cnt;
	u32_t optionArrayLength;
	u8_t optionIdx;
	err_t status;
	u8_t clientIterationDoneFlag;

	if (entrySubscribeCnt > 0) {
		optionIpv4Cnt = entrySubscribeCnt;  // maximal possible counts
		optionArrayLength = optionIpv4Cnt * sizeof(struct sdOptionIpv4_t);
		pbOption = pbuf_alloc(PBUF_TRANSPORT, optionArrayLength, PBUF_POOL);
	} else {
		optionIpv4Cnt = 0;
		pbOption = NULL;
	}

	entryArrayLength = (entrySubscribeCnt + entrySubscribeAckCnt) * sizeof(struct sdEntry_t);
	pbSdMsg = pbuf_alloc(PBUF_TRANSPORT, SOMEIP_SD_MIN_LENGTH_ALL + entryArrayLength + optionArrayLength, PBUF_POOL);
	currentSdMsgPtr = pbSdMsg->payload;

	sdMessageInit(&currentSdMsgPtr, deviceInterface->sessionId, gRebootFlag);
	someip_sessionIdInc(deviceInterface->sessionId);
	sdHdr_set_arrayLength(currentSdMsgPtr, entryArrayLength);
	currentSdMsgPtr += sizeof(u32_t);

	optionIpv4Cnt = 0;
	clientIterationDoneFlag = (clientServices != NULL) ? 0 : 1;
	for (service = (clientServices != NULL) ? clientServices : serverServices; service != NULL;) {
		if (service->flagToSubscribe) {
			addOptionIpv4((u8_t *)(pbOption->payload), service->unicastSrcDevice, &optionIpv4Cnt, &optionIdx, SD_OT_IPV4_ENDPOINT);
		}

		for (method = service->methods; method != NULL; method = method->next) {
			if ((service->flagToSubscribe) || (method->flagToSubscribeAckAndTtl)) {
				entry = (struct sdEntry_t *)currentSdMsgPtr;
				MEMCPY((u8_t *)entry, (u8_t *)(&entryBasic), sizeof(struct sdEntry_t));
				currentSdMsgPtr += sizeof(struct sdEntry_t);

				sdEntry_set_serviceId(entry, service->id);
				sdEntry_set_flagCounter(entry, 0, 0);
				sdEntry_set_eventgroupId(entry, method->id);

				if (service->flagToSubscribe) {
					sdEntry_set_type(entry, SD_ET_SUBSCRIBE);
					sdEntry_set_idx1option(entry, optionIdx);
					sdEntry_set_numOfOptions(entry, 1, 0);
					sdEntry_set_ttl(entry, SD_TTL_SUBSCRIBE);
				}
				else if (method->flagToSubscribeAckAndTtl) {
					sdEntry_set_type(entry, SD_ET_SUBSCRIBE_ACK);
					sdEntry_set_ttl(entry, method->flagToSubscribeAckAndTtl);
				}
			}
			method->flagToSubscribeAckAndTtl = 0;
		}
		service->flagToSubscribe = 0;


		if ((service->next == NULL) && (!clientIterationDoneFlag)) {
			clientIterationDoneFlag = 1;
			service = serverServices;
		}
		else {
			service = service->next;
		}
	}

	optionArrayLength = optionIpv4Cnt * sizeof(struct sdOptionIpv4_t);
	sdHdr_set_arrayLength(currentSdMsgPtr, optionArrayLength);
	currentSdMsgPtr += sizeof(u32_t);

	if (entrySubscribeCnt > 0) {
		MEMCPY((u8_t *)currentSdMsgPtr, (u8_t *)(pbOption->payload), optionArrayLength);
		pbuf_free(pbOption);
		pbuf_realloc(pbSdMsg, SOMEIP_SD_MIN_LENGTH_ALL + entryArrayLength + optionArrayLength);
	}

	someipUpdateLength(pbSdMsg);

	status = udp_sendto(someipSdService->unicastSrcDevice->udpPcb, pbSdMsg, &(deviceInterface->ip4Addr), SD_PORT);
	pbuf_free(pbSdMsg);

	return status;
}


/**
 * Send SD message with Find or Offer service information
 * @param pbMsgOriginal Pointer to pointer to buffer with current version of Find or Offer service message
 * @return ERR_OK if message was send properly, otherwise err_t error code
 */
static err_t sendFindOfferService(struct pbuf *pbMsgOriginal)
{
	struct pbuf *pb;
	err_t status;

	if (pbMsgOriginal == NULL) {
		return ERR_ARG;
	}

	pb = pbuf_alloc(PBUF_TRANSPORT, pbMsgOriginal->len, PBUF_POOL);
	MEMCPY((u8_t *)(pb->payload), (u8_t *)(pbMsgOriginal->payload), pbMsgOriginal->len);
	status = udp_sendto(someipSdService->unicastSrcDevice->udpPcb, pb, &(gMulticastDeviceInterface->ip4Addr), SD_PORT);
	if (status != ERR_OK) {
		return status;
	}
	pbuf_free(pb);
	someip_set_sessionId(pbMsgOriginal->payload, gMulticastDeviceInterface->sessionId);
	someip_sessionIdInc(gMulticastDeviceInterface->sessionId);

	return ERR_OK;
}


/**
 * Update buffer with prepared frame for sending find and offer service message. This function is
 * call every time when some service is added or deleted from singly linked list of services
 * @param pbSdMsgOld Pointer to pointer to buffer with previous version of find or offer service message
 * @param service Pointer to service, which should be add to find or offer service message
 * @return ERR_OK if update was performed properly, otherwise err_t error code
 */
static err_t updateFindOfferService(struct pbuf **pbSdMsgOld, struct someip_service_t *service)
{
	u8_t *sdMsgEntryPtrOld, *sdMsgOptionPtrOld, *sdMsgEntryPtrNew, *sdMsgOptionPtrNew;
	u32_t entryArrayLengthOld, optionArrayLengthOld, entryArrayLengthNew, optionArrayLengthNew;
	struct pbuf *pbSdMsgNew;
	struct sdEntry_t *entry;
	u16_t optionIpv4Cnt;
	u8_t optionIdx;

	if ((pbSdMsgOld == NULL) || (service == NULL)) {
		return ERR_ARG;
	}

	sdMsgEntryPtrOld = (*pbSdMsgOld)->payload + sizeof(struct someipHdr_t) + sizeof(struct sdHdr_t);
	entryArrayLengthOld = sdHdr_arrayLength(sdMsgEntryPtrOld);

	sdMsgOptionPtrOld = sdMsgEntryPtrOld + sizeof(u32_t) + entryArrayLengthOld;
	optionArrayLengthOld = sdHdr_arrayLength(sdMsgOptionPtrOld);

	pbSdMsgNew = pbuf_alloc(PBUF_TRANSPORT, SOMEIP_SD_MIN_LENGTH_ALL + entryArrayLengthOld + sizeof(struct sdEntry_t) + optionArrayLengthOld + sizeof(struct sdOptionIpv4_t), PBUF_POOL);

	MEMCPY((u8_t *)(pbSdMsgNew->payload), (u8_t *)(*pbSdMsgOld)->payload, SOMEIP_SD_MIN_LENGTH_ALL + entryArrayLengthOld - sizeof(u32_t) /* without options array length field */);

	sdMsgEntryPtrNew = pbSdMsgNew->payload + sizeof(struct someipHdr_t) + sizeof(struct sdHdr_t);
	entryArrayLengthNew = entryArrayLengthOld + sizeof(struct sdEntry_t);
	sdHdr_set_arrayLength(sdMsgEntryPtrNew, entryArrayLengthNew);

	sdMsgEntryPtrNew += sizeof(u32_t) + entryArrayLengthOld;
	entry = (struct sdEntry_t *)sdMsgEntryPtrNew;
	MEMCPY((u8_t *)entry, (u8_t *)(&entryBasic), sizeof(struct sdEntry_t));

	sdMsgOptionPtrNew = sdMsgEntryPtrNew + sizeof(struct sdEntry_t);
	MEMCPY((u8_t *)sdMsgOptionPtrNew, (u8_t *)sdMsgOptionPtrOld, sizeof(u32_t) + optionArrayLengthOld);

	sdEntry_set_serviceId(entry, service->id);

	if (*pbSdMsgOld == gPbFindService) {
		sdEntry_set_type(entry, SD_ET_FIND_SERVICE);
		sdEntry_set_ttl(entry, gCyclicFindDelayMs + SOMEIP_SEC_IN_MS);
	}
	else if (*pbSdMsgOld == gPbOfferService) {
		optionIpv4Cnt = optionArrayLengthOld / sizeof(struct sdOptionIpv4_t);
		addOptionIpv4(sdMsgOptionPtrNew + sizeof(u32_t), service->unicastSrcDevice, &optionIpv4Cnt, &optionIdx, SD_OT_IPV4_ENDPOINT);

		sdEntry_set_type(entry, SD_ET_OFFER_SERVICE);
		sdEntry_set_ttl(entry, gCyclicOfferDelayMs + SOMEIP_SEC_IN_MS);
		sdEntry_set_idx1option(entry, optionIdx);
		sdEntry_set_numOfOptions(entry, 1, 0);

		optionArrayLengthNew = optionIpv4Cnt * sizeof(struct sdOptionIpv4_t);
		sdHdr_set_arrayLength(sdMsgOptionPtrNew, optionArrayLengthNew);

	}

	pbuf_realloc(pbSdMsgNew, SOMEIP_SD_MIN_LENGTH_ALL + entryArrayLengthNew + optionArrayLengthNew);
	someipUpdateLength(pbSdMsgNew);

	pbuf_free(*pbSdMsgOld);
	*pbSdMsgOld = pbSdMsgNew;

	return ERR_OK;
}


/**
 * Perform Acknowledge for client subscription on server side - create appropriate device interface
 * and set TTL to specified method structure
 * @param entrySubscribeCnt Pointer to counter used for estimate length of response
 * @param method Pointer to subscribed method in Server singly linked list of methods in subscribed service.
 * @param entry Pointer to entry from received Entries Array with information about service and TTL
 * @param option1 Pointer to first option from received Options Array with information about addressing
 * @param option1 Pointer to second option from received Options Array with additional information
 */
static void subscribeAckMethod(u16_t *entrySubscribeCnt, struct someip_method_t *method, struct sdEntry_t *entry, void *option1, void *option2)
{
	struct sdOptionIpv4_t * optionIpv4;
	struct ip4_addr ip4Addr;
	struct someip_deviceInterface_t *newDeviceInterface;

	optionIpv4 = (struct sdOptionIpv4_t *)option1;
	ip4_addr_set_u32(&ip4Addr, sdOptionIpv4_address(optionIpv4));

	method->flagToSubscribeAckAndTtl = sdEntry_ttl(entry);
	addDeviceInterface(&(method->dstDevices), &newDeviceInterface, &ip4Addr, sdOptionIpv4_port(optionIpv4), sdOptionIpv4_protocol(optionIpv4), sdEntry_ttl(entry), SOMEIP_RECEIVER);
	++(*entrySubscribeCnt);

}


/**
 * Perform subscription for service on client side, create appropriate device interface and set TTL
 * @param entrySubscribeCnt Pointer to counter used for estimate length of response
 * @param service Pointer to subscribed server in Client singly linked list of services.
 * @param entry Pointer to entry from received Entries Array with information about service and TTL
 * @param option1 Pointer to first option from received Options Array with information about addressing
 * @param option1 Pointer to second option from received Options Array with additional information
 */
static void subscribeService(u16_t *entrySubscribeCnt, struct someip_service_t *service, struct sdEntry_t *entry, void *option1, void *option2)
{
	struct sdOptionIpv4_t * optionIpv4;
	struct ip4_addr ip4Addr;
	struct someip_method_t *method;
	struct someip_deviceInterface_t *newDeviceInterface;

	optionIpv4 = (struct sdOptionIpv4_t *)option1;
	ip4_addr_set_u32(&ip4Addr, sdOptionIpv4_address(optionIpv4));

	service->flagToSubscribe = 1;
	for (method = service->methods; method != NULL; method = method->next) {
		addDeviceInterface(&(service->methods->dstDevices), &newDeviceInterface, &ip4Addr, sdOptionIpv4_port(optionIpv4), sdOptionIpv4_protocol(optionIpv4), sdEntry_ttl(entry), SOMEIP_RECEIVER);
		++(*entrySubscribeCnt);
	}



}


/**
 * Function to get option from SD message in spite of their type
 * @param optionsPtr Pointer to buffer with received option array
 * @param option1 Pointer to pointer option in option array
 * @return ERR_OK if get option was performed properly, otherwise err_t error code
 */
static err_t getOption(u8_t *optionsPtr, void **option)
{
	if ((optionsPtr == NULL) || (option == NULL)) {
		return ERR_ARG;
	}


	if ((sdHdr_optionType(optionsPtr) == SD_OT_IPV4_ENDPOINT)
			|| (sdHdr_optionType(optionsPtr) == SD_OT_IPV4_MULTICAST)
			|| (sdHdr_optionType(optionsPtr) == SD_OT_IPV4_SD_ENDPOINT)) {
		*option = (struct sdOptionIpv4_t *)optionsPtr;
	}
	else if ((sdHdr_optionType(optionsPtr) == SD_OT_IPV6_ENDPOINT)
			|| (sdHdr_optionType(optionsPtr) == SD_OT_IPV6_MULTICAST)
			|| (sdHdr_optionType(optionsPtr) == SD_OT_IPV6_SD_ENDPOINT)) {
		*option = (struct sdOptionIpv6_t *)optionsPtr;
	}
	else if (sdHdr_optionType(optionsPtr) == SD_OT_CONFIGURATION) {
		*option = (struct sdOptionConf_t *)optionsPtr;
	}
	else if (sdHdr_optionType(optionsPtr) == SD_OT_LOAD_BALANCING) {
		*option = (struct sdOptionLoad_t *)optionsPtr;
	}
	else {
		// unknown option type
		return ERR_VAL;
	}

	return ERR_OK;
}


/**
 * Function to encapsulate first and second options from received SD message
 * @param optionsPtr Pointer to buffer with received option array
 * @param entry Pointer to received entry
 * @param option1 Pointer to pointer to first option in option array
 * @param option2 Pointer to pointer to second option in option array
 * @return ERR_OK if get options was performed properly, otherwise err_t error code
 */
static err_t getOptions(u8_t *optionsPtr, struct sdEntry_t *entry, void **option1, void **option2)
{
	u32_t optionsArrayLength, optionsArrayOffset;
	u16_t optionLength;
	u8_t optionIdx;
	err_t status;

	if ((optionsPtr == NULL) || (entry == NULL) || (option1 == NULL) || (option2 == NULL)) {
		return ERR_ARG;
	}

	optionsArrayLength = sdHdr_arrayLength(optionsPtr);
	if (optionsArrayLength > 0) {
		optionsPtr += sizeof(u32_t);
	}
	else {
		return ERR_VAL;
	}

	optionLength = 0;
	optionIdx = 0;
	for (optionsArrayOffset = 0; optionsArrayOffset < optionsArrayLength; optionsArrayOffset += optionLength) {
		if ((sdEntry_numOfOptions1(entry) > 0) && (sdEntry_idx1option(entry) == optionIdx)) {
			if (ERR_OK != (status = getOption(optionsPtr, option1))) {
				return status;
			}
		}
		if ((sdEntry_numOfOptions2(entry) > 0) && (sdEntry_idx2option(entry) == optionIdx)) {
			if (ERR_OK != (status = getOption(optionsPtr, option2))) {
				return status;
			}
		}
		optionLength = sdHdr_optionLength(optionsPtr) + SD_OT_LEN_TYPE_SIZE;
		optionsPtr += optionLength;
		++optionIdx;
	}

	return ERR_OK;
}


/**
 * Function to check if received SD message has proper proper lenght
 * @return ERR_OK if SOME/IP message has proper header format, otherwise err_t error code
 */
static err_t errorHandlingSd(u32_t someipLength, struct pbuf *pb)
{
//	struct sdHdr_t *header;
//	header = pb->payload;
//	// Check Reboot Flag, Unicast Flag, Explicit Initial Data Control Flag

	if ((someipLength < SOMEIP_SD_MIN_LENGTH) || (pb->len < SD_MIN_LENGTH)) {
		return ERR_VAL;
	}

	return ERR_OK;
}


/**
 * Function call during receive SOME/IP-SD message.
 * @param someipLength Length of received SOME/IP-SD message
 * @param sessionId Session ID of received SOME/IP-SD message
 * @param pbRequest Buffer contains SD message
 * @param responseDstAddr Pointer to IP address which will be used to response on this SD message
 */
static void recvSd(u32_t someipLength, u16_t sessionId, struct pbuf *pbRequest, ip_addr_t *responseDstAddr)
{
	u32_t entriesArrayLength;
	u8_t *currentBufferPtr, *optionsStartPtr;
	u32_t entriesArrayOffset;
	err_t status;
	struct sdEntry_t *entry;
	void *option1, *option2;
	struct someip_service_t *service;
	struct someip_method_t *method;
	struct someip_deviceInterface_t *newDeviceInterface;
	u16_t entrySubscribeCnt, entrySubscribeAckCnt;

	if (errorHandlingSd(someipLength, pbRequest) == ERR_OK) {
		pbuf_remove_header(pbRequest, sizeof(struct sdHdr_t));

		currentBufferPtr = pbRequest->payload;
		entriesArrayLength = sdHdr_arrayLength(currentBufferPtr);
		currentBufferPtr += sizeof(u32_t);
		optionsStartPtr = currentBufferPtr + entriesArrayLength;

		entrySubscribeCnt = 0;
		entrySubscribeAckCnt = 0;
		for (entriesArrayOffset = 0; entriesArrayOffset < entriesArrayLength; entriesArrayOffset += sizeof(struct sdEntry_t)) {
			entry = (struct sdEntry_t *)currentBufferPtr;
			if (sdEntry_type(entry) == SD_ET_FIND_SERVICE) {
				if (serverServices != NULL) {
					sendFindOfferService(gPbOfferService);
				}
			}
			else if (sdEntry_type(entry) == SD_ET_OFFER_SERVICE) {
				for (service = clientServices; service != NULL; service = service->next) {
					if ((service->id == sdEntry_serviceId(entry)) && (service->methods != NULL)) {
						status = getOptions(optionsStartPtr, entry, &option1, &option2);
						if (status == ERR_OK) {
							subscribeService(&entrySubscribeCnt, service, entry, option1, option2);
						}
					}
				}
			}
			else if (sdEntry_type(entry) == SD_ET_SUBSCRIBE) {
				for (service = serverServices; service != NULL; service = service->next) {
					if ((service->id == sdEntry_serviceId(entry)) && (service->methods != NULL)) {
						for (method = service->methods; method != NULL; method = method->next) {
							if (method->id == sdEntry_eventgroupId(entry)) {
								status = getOptions(optionsStartPtr, entry, &option1, &option2);
								if (status == ERR_OK) {
									subscribeAckMethod(&entrySubscribeCnt, method, entry, option1, option2);
								}
							}
						}
					}
				}
			}
			else if (sdEntry_type(entry) == SD_ET_SUBSCRIBE_ACK) {
				// Client reaction for receive subscribe ack - not used in UDP version
			}
			currentBufferPtr += sizeof(struct sdEntry_t);
		}

		if ((entrySubscribeCnt > 0) || (entrySubscribeAckCnt > 0)) {
			status = addDeviceInterface(&(someipSdService->methods->dstDevices), &newDeviceInterface, responseDstAddr, SD_PORT, SOMEIP_PROTOCOL_UDP, 0, SOMEIP_RECEIVER);
			if (status == ERR_OK) {
				sendSubscribeAndSubscribeAck(entrySubscribeCnt, entrySubscribeAckCnt, newDeviceInterface);
			}
		}
	}

	pbuf_free(pbRequest);
}


/**
 * Function to check if received SOME/IP message has proper protocol version,
 * interface version return code and length value.
 * @return ERR_OK if SOME/IP message has proper header format, otherwise err_t error code
 */
static err_t errorHandlingSomeip(struct pbuf *pb)
{
	struct someipHdr_t *header;

	gRceiveMsgErrorCode = SOMEIP_EC_OK;

	header = pb->payload;

	if ((pb->len < SOMEIP_MIN_LENGTH_ALL) || (header->length < SOMEIP_MIN_LENGTH)) {
		gRceiveMsgErrorCode = SOMEIP_EC_MALFORMED_MESSAGE;
		return ERR_VAL;
	}
	else if (header->protocolVersion != SOMEIP_PROTOCOL_VERSION) {
		gRceiveMsgErrorCode = SOMEIP_EC_WRONG_PROTOCOL_VERSION;
		return ERR_VAL;
	}
//	else if (header->interfaceVersion != SOMEIP_INTERFACE_VERSION) {
//		gRceiveMsgErrorCode = SOMEIP_EC_WRONG_INTERFACE_VERSION;
//		return ERR_VAL;
//	}
	else if ((header->returnCode < 0x00) || (header->returnCode >= 0x20)) {
		gRceiveMsgErrorCode = SOMEIP_EC_MALFORMED_MESSAGE;
		return ERR_VAL;
	}

	return ERR_OK;
}


/**
 * Callback from UDP binded PCB which is call when SOME/IP message was received.
 */
static void recvSomeip(void *arg, struct udp_pcb *pcb, struct pbuf *pb, const ip_addr_t *srcAddr, u16_t port)
{
	struct someipHdr_t *header;
	u8_t clientIterationDoneFlag, serviceFoundFlag, methodFoundFlag;
	struct someip_service_t *service;
	struct someip_method_t *method;
	struct pbuf *pbRequest;

	header = pb->payload;
	if (errorHandlingSomeip(pb) == ERR_OK) {

		serviceFoundFlag = 0;
		methodFoundFlag = 0;
		if ((someipHdr_serviceId(header) == SD_SERVICE_ID) && (someipHdr_methodId(header) == SD_METHOD_ID)) {
			serviceFoundFlag = 1;
			methodFoundFlag = 1;
			if (gUserSdCallbackFcn == NULL) {
				pbuf_remove_header(pb, sizeof(struct someipHdr_t));
				recvSd(someipHdr_length(header), someipHdr_sessionId(header), pb, (ip_addr_t *)srcAddr);
			}
			else {
				pbuf_remove_header(pb, sizeof(struct someipHdr_t));
				gUserSdCallbackFcn(pb, srcAddr, port, someipHdr_serviceId(header), someipHdr_methodId(header));
			}
		}
		else {
			clientIterationDoneFlag = (clientServices != NULL) ? 0 : 1;
			for (service = (clientServices != NULL) ? clientServices : serverServices; service != NULL;) {
				if (someipHdr_serviceId(header) == service->id) {
					serviceFoundFlag = 1;
					for (method = service->methods; method != NULL; method = method->next) {
						if (((someipHdr_methodId(header) & SOMEIP_METHOD_ID_MASK) == method->id) && (method->callbackFcn != NULL)) {
							methodFoundFlag = 1;
							pbuf_remove_header(pb, sizeof(struct someipHdr_t));
							method->callbackFcn(pb, srcAddr, port, someipHdr_serviceId(header), someipHdr_methodId(header));
						}
					}
				}
				if ((service->next == NULL) && (!clientIterationDoneFlag)) {
					clientIterationDoneFlag = 1;
					service = serverServices;
				}
				else {
					service = service->next;
				}
			}
		}

		if (!serviceFoundFlag) {
			gRceiveMsgErrorCode = SOMEIP_EC_UNKNOWN_SERVICE;
		}
		else if (!methodFoundFlag) {
			gRceiveMsgErrorCode = SOMEIP_EC_UNKNOWN_METHOD;
		}
	}

	if ((gRceiveMsgErrorCode != SOMEIP_EC_OK) && ((header->messageType == SOMEIP_MT_REQUEST) || (header->messageType == SOMEIP_MT_TP_REQUEST))) {
		pbRequest = pbuf_alloc(PBUF_TRANSPORT, SOMEIP_MIN_LENGTH_ALL, PBUF_POOL);
		MEMCPY((u8_t *)(pbRequest->payload), (u8_t *)header, SOMEIP_MIN_LENGTH_ALL);
		udp_sendto(pcb, pbRequest, srcAddr, port);
		pbuf_free(pbRequest);
	}

	if (gRceiveMsgErrorCode != SOMEIP_EC_OK) {
		pbuf_free(pb);
	}


}


/**
 * Add device interface to singly linked list contains previously defined device interfaces
 * @param firstDeviceInterface Pointer to pointer to first device interface in singly linked list of device interfaces
 * @param newDeviceInterface Pointer to pointer to device interface which will be added to singly linked list of device interfaces
 * @param ip4Addr IPv4 address which will be connected with this device interface
 * @param port Port number which will be connected with this device interface
 * @param ttl Time to live of this interface, if it is equal to 0 this device interface will be deleted. If this value is negative this interface will not be removed (see device interface of SD service and method).
 * @param direction Direction of sending data if this interface will be used to send messages (transciver) the special PCB will be binded
 * @return ERR_OK if device interface was added properly otherwise err_t error code
 */
static err_t addDeviceInterface(struct someip_deviceInterface_t **firstDeviceInterface, struct someip_deviceInterface_t **newDeviceInterface, ip4_addr_t *ip4Addr, u16_t port, u8_t protocol, s32_t ttl, enum someip_dir_t direction)
{
	struct someip_deviceInterface_t *deviceInterface;
	struct udp_pcb *pcb, *ipcb;
	err_t status;

	if ((firstDeviceInterface == NULL) && (newDeviceInterface == NULL) && (ip4Addr == NULL)) {
		return ERR_ARG;
	}

	for (deviceInterface = *firstDeviceInterface; deviceInterface != NULL; deviceInterface = deviceInterface->next) {
		if ((ip4_addr_cmp(&deviceInterface->ip4Addr, ip4Addr))
				&& (deviceInterface->port == port)
				&& (deviceInterface->protocol == protocol)) {
			*newDeviceInterface = deviceInterface;
			break;  // deviceInterface already exists
		}
	}

	if (deviceInterface == NULL) { // deviceInterface not exists
		deviceInterface = mem_malloc(sizeof(struct someip_deviceInterface_t));
		if (deviceInterface == NULL) {
			return ERR_MEM;
		}

		deviceInterface->next = *firstDeviceInterface;
		*firstDeviceInterface = deviceInterface;
		*newDeviceInterface = deviceInterface;

		ip4_addr_copy(deviceInterface->ip4Addr, *ip4Addr);
		deviceInterface->port = port;
		deviceInterface->protocol = protocol;
		deviceInterface->sessionId = 1;


		if (direction == SOMEIP_TRANSCEIVER) {
			pcb = udp_new();
			status = udp_bind(pcb, ip4Addr, port);
			if (status == ERR_USE) {
				for (ipcb = udp_pcbs; ipcb != NULL; ipcb = ipcb->next) {
					if ((ipcb->local_port == port) && (ip_addr_cmp(&ipcb->local_ip, ip4Addr))) {
						deviceInterface->udpPcb = ipcb;
						status = ERR_OK;
						break;
					}
				}
			}

			if (status != ERR_OK) {
				return status;
			}

			udp_recv(pcb, recvSomeip, NULL);

			deviceInterface->udpPcb = pcb;
		}
		else {
			deviceInterface->udpPcb = NULL;
		}
	}

	deviceInterface->ttl = ttl;

	return ERR_OK;
}


/**
 * Remove device interface from singly linked list contains previously defined device interfaces
 * @param firstDeviceInterface Pointer to pointer to first device interface in singly linked list of device interfaces
 * @param prevDeviceInterface Pointer to device interface which is behind device interface which will be deleted from singly linked list of device interfaces
 * @param deviceInterface Pointer to device interface which will be deleted from singly linked list of device interfaces
 * @return ERR_OK if device interface was deleted properly otherwise err_t error code
 */
static err_t removeDeviceInterface(struct someip_deviceInterface_t **firstDeviceInterface, struct someip_deviceInterface_t *prevDeviceInterface, struct someip_deviceInterface_t *deviceInterface)
{
	if ((firstDeviceInterface == NULL) && (deviceInterface == NULL)) {
		return ERR_ARG;
	}

	if (prevDeviceInterface) {
		prevDeviceInterface->next = deviceInterface->next;
	}
	else {
		*firstDeviceInterface = deviceInterface->next;
	}

	mem_free(deviceInterface);

	return ERR_OK;
}


/**
 * Add method to previously created service.
 * @param service Pointer to previously created service structure
 * @param newMethod Pointer to pointer to previously created method structure
 * @param id ID of method that will be added
 * @param callbackFcn Pointer to function which will be call when message with such service ID and method ID will be received
 * @return ERR_OK if method was added properly otherwise err_t error code
 */
static err_t addMethod(struct someip_service_t *service, struct someip_method_t **newMethod, u16_t id, someipCallback_t callbackFcn)
{
	struct someip_method_t *method;

	if ((service == NULL) || (newMethod == NULL)) {
		return ERR_ARG;
	}

	method = mem_malloc(sizeof(struct someip_method_t));
	if (method == NULL) {
		return ERR_MEM;
	}

	method->next = service->methods;
	service->methods = method;

	method->id = id;
	method->dstDevices = NULL;
	method->callbackFcn = callbackFcn;
	method->flagToSubscribeAckAndTtl = 0;

	*newMethod = method;

	return ERR_OK;
}


/**
 * Remove method from previously created service.
 * @param service Pointer to previously created service structure
 * @param methodId ID of method that will be removed
 * @return ERR_OK if method was deleted properly otherwise err_t error code
 */
static err_t removeMethod(struct someip_service_t *service, u16_t methodId)
{
	struct someip_method_t *prevMethod, *currMethod;
	err_t status;

	prevMethod = NULL;
	for (currMethod = service->methods; currMethod != NULL; currMethod = currMethod->next) {
		if (currMethod->id == methodId) {
			while (currMethod->dstDevices != NULL) {
				status = removeDeviceInterface(&(currMethod->dstDevices), NULL, currMethod->dstDevices);
				if (status != ERR_OK) {
					return status;
				}
			}

			if (prevMethod) {
				prevMethod->next = currMethod->next;
			}
			else {
				service->methods = currMethod->next;
			}

			mem_free(currMethod);

			break;
		}

		prevMethod = currMethod;
	}

	return ERR_OK;
}


/**
 * Add service to client or server singly linked list contains previously defined services.
 * @param firstService Pointer to pointer of first service structure in client or server singly linked list contains previously defined services
 * @param newService Pointer to pointer of new created service structure
 * @param id ID of added service
 * @param serviceIpv4Addr Pointer to IPv4 address of service localization
 * @param servicePort Value of port number of service localization
 * @param serviceProtocol Type of protocol which will be used to communicate with this service defined in someip.h file as #define SOMEIP_PROTOCOL_
 * @param ttl Time to live which will be send during find or offer service SD message in seconds
 * @return ERR_OK if service was added properly otherwise err_t error code
 */
static err_t addService(struct someip_service_t **firstService, struct someip_service_t **newService, u16_t id, ip4_addr_t *serviceIpv4Addr, u16_t servicePort, u8_t serviceProtocol, s32_t ttl)
{
	struct someip_service_t *service;
	struct someip_deviceInterface_t *newDeviceInterface;
	err_t status;

	service = mem_malloc(sizeof(struct someip_service_t));
	if (service == NULL) {
		return ERR_MEM;
	}

	service->next = *firstService;
	*firstService = service;

	service->id = id;
	service->methods = NULL;
	service->unicastSrcDevice = NULL;
	service->multicastSrcDevice = NULL;
	service->flagToSubscribe = 0;

	status = addDeviceInterface(&(service->unicastSrcDevice), &newDeviceInterface, serviceIpv4Addr, servicePort, serviceProtocol, SD_TTL_INF, SOMEIP_TRANSCEIVER);
	if (status != ERR_OK) {
		return status;
	}

	if (*firstService == clientServices) {
		status = updateFindOfferService(&gPbFindService, service);
	}
	else if (*firstService == serverServices) {
		status = updateFindOfferService(&gPbOfferService, service);
	}

	if (status != ERR_OK) {
		return status;
	}

	*newService = service;

	return ERR_OK;
}


/**
 * Remove service from client or server singly linked list contains previously defined services.
 * @param firstService Pointer to pointer of first service structure in client or server singly linked list contains previously defined services
 * @param serviceId ID of deleted service
 * @return ERR_OK if service was deleted properly otherwise err_t error code
 */
static err_t removeService(struct someip_service_t **firstService, u16_t serviceId)
{
	struct someip_service_t *currService, *prevService;
	err_t status;

	prevService = NULL;
	for (currService = *firstService; currService != NULL; currService = currService->next) {
		if (currService->id == serviceId) {
			while (currService->methods) {
				status = removeMethod(currService, currService->methods->id);
				if (status != ERR_OK) {
					return status;
				}
			}

			while (currService->unicastSrcDevice != NULL) {
				status = removeDeviceInterface(&(currService->unicastSrcDevice), NULL, currService->unicastSrcDevice);
				if (status != ERR_OK) {
					return status;
				}
			}

			while (currService->multicastSrcDevice != NULL) {
				status = removeDeviceInterface(&(currService->multicastSrcDevice), NULL, currService->multicastSrcDevice);
				if (status != ERR_OK) {
					return status;
				}
			}

			if (prevService) {
				prevService->next = currService->next;
			}
			else {
				*firstService = currService->next;
			}

			mem_free(currService);

			break;
		}
		prevService = currService;
	}


	return ERR_OK;
}


/**
 * Add method to previously created service.
 * @param service Pointer to previously created service structure
 * @param methodId ID of method that will be added
 * @param callbackFcn Pointer to function which will be call when message with such service ID and method ID will be received
 * @return ERR_OK if method was added properly otherwise err_t error code
 */
err_t someip_addMethod(struct someip_service_t *service, u16_t methodId, someipCallback_t callbackFcn)
{
	struct someip_method_t *newMethod;
	err_t status;

	status = addMethod(service, &newMethod, methodId, callbackFcn);
	if (status != ERR_OK) {
		return status;
	}

	return ERR_OK;
}


/**
 * Remove method from previously created service.
 * @param service Pointer to previously created service structure
 * @param methodId ID of method that will be removed
 * @return ERR_OK if method was deleted properly otherwise err_t error code
 */
err_t someip_removeMethod(struct someip_service_t *service, u16_t methodId)
{
	return removeMethod(service, methodId);
}


/**
 * Add service to client singly linked list contains previously defined services.
 * @param newService Pointer to pointer to new created service structure
 * @param serviceId ID of added service
 * @param serviceIpv4Addr Pointer to IPv4 address of service localization
 * @param servicePort Value of port number of service localization
 * @param serviceProtocol Type of protocol which will be used to communicate with this service defined in someip.h file as #define SOMEIP_PROTOCOL_
 * @return ERR_OK if service was added properly otherwise err_t error code
 */
err_t someip_addClientService(struct someip_service_t **newService, u16_t serviceId, ip4_addr_t *serviceIpv4Addr, u16_t servicePort, u8_t serviceProtocol)
{
	return addService(&clientServices, newService, serviceId, serviceIpv4Addr, servicePort, serviceProtocol, SD_TTL_INF);
}


/**
 * Add service to server singly linked list contains previously defined services.
 * @param newService Pointer to pointer to new created service structure
 * @param serviceId ID of added service
 * @param serviceIpv4Addr Pointer to IPv4 address of service localization
 * @param servicePort Value of port number of service localization
 * @param serviceProtocol Type of protocol which will be used to communicate with this service defined in someip.h file as #define SOMEIP_PROTOCOL_
 * @return ERR_OK if service was added properly otherwise err_t error code
 */
err_t someip_addServerService(struct someip_service_t **newService, u16_t serviceId, ip4_addr_t *serviceIpv4Addr, u16_t servicePort, u8_t serviceProtocol)
{
	return addService(&serverServices, newService, serviceId, serviceIpv4Addr, servicePort, serviceProtocol, SD_TTL_INF);
}


/**
 * Remove service from client singly linked list contains previously defined services.
 * @param serviceId ID of deleted service
 * @return ERR_OK if service was deleted properly otherwise err_t error code
 */
err_t someip_removeClientService(u16_t serviceId)
{
	return removeService(&clientServices, serviceId);
}


/**
 * Remove service from server singly linked list contains previously defined services.
 * @param serviceId ID of deleted service
 * @return ERR_OK if service was deleted properly otherwise err_t error code
 */
err_t someip_removeServerService(u16_t serviceId)
{
	return removeService(&serverServices, serviceId);
}


/**
 * Initialization of Service Discovery service.
 * @param ip4AddrLocalHost Pointer to local host IPv4 address
 * @param ip4AddrMulticast Pointer to multicast group IPv4 address
 * @return ERR_OK if everything was initialize properly otherwise err_t error code
 */
static err_t initSdService(ip4_addr_t *localHostIp4Addr, ip4_addr_t *multicastIp4Addr)
{
	err_t status;
	struct someip_service_t *newService;
	struct someip_method_t *newMethod;
	struct someip_deviceInterface_t *newDeviceInterface;

	status = addService(&someipSdService, &newService, SD_SERVICE_ID, localHostIp4Addr, SD_PORT, SOMEIP_PROTOCOL_UDP, SD_TTL_INF);
	if (status != ERR_OK) {
		return status;
	}

	status = addDeviceInterface(&(newService->multicastSrcDevice), &newDeviceInterface, multicastIp4Addr, SD_PORT, SOMEIP_PROTOCOL_UDP, SD_TTL_INF, SOMEIP_TRANSCEIVER);
	if (status != ERR_OK) {
		return status;
	}

	status = addMethod(newService, &newMethod, SD_METHOD_ID, NULL);
	if (status != ERR_OK) {
		return status;
	}

	status = addDeviceInterface(&(newMethod->dstDevices), &newDeviceInterface, multicastIp4Addr, SD_PORT, SOMEIP_PROTOCOL_UDP, SD_TTL_INF, SOMEIP_RECEIVER);
	if (status != ERR_OK) {
		return status;
	}

	gMulticastDeviceInterface = newDeviceInterface;

	return ERR_OK;
}


/**
 * Initialization of basic SD structures.
 */
static void initSdBasicStructures(void)
{
	someipHdr_set_serviceId(&someipHdrBasic, 0);
	someipHdr_set_methodId(&someipHdrBasic, 0);
	someipHdr_set_length(&someipHdrBasic, SOMEIP_MIN_LENGTH);
	someipHdr_set_clientId(&someipHdrBasic, 0);
	someipHdr_set_sessionId(&someipHdrBasic, 1);
	someipHdr_set_protocolVersion(&someipHdrBasic, SOMEIP_PROTOCOL_VERSION);
	someipHdr_set_interfaceVersion(&someipHdrBasic, SOMEIP_INTERFACE_VERSION);
	someipHdr_set_messageType(&someipHdrBasic, SOMEIP_MT_NOTIFICATION);
	someipHdr_set_returnCode(&someipHdrBasic, SOMEIP_EC_OK);

	someipHdr_set_serviceId(&someipSdHdrBasic, SD_SERVICE_ID);
	someipHdr_set_methodId(&someipSdHdrBasic, SD_METHOD_ID);
	someipHdr_set_length(&someipSdHdrBasic, SOMEIP_SD_MIN_LENGTH);
	someipHdr_set_clientId(&someipSdHdrBasic, 0);
	someipHdr_set_sessionId(&someipSdHdrBasic, 1);
	someipHdr_set_protocolVersion(&someipSdHdrBasic, SOMEIP_PROTOCOL_VERSION);
	someipHdr_set_interfaceVersion(&someipSdHdrBasic, SOMEIP_INTERFACE_VERSION);
	someipHdr_set_messageType(&someipSdHdrBasic, SOMEIP_MT_NOTIFICATION);
	someipHdr_set_returnCode(&someipSdHdrBasic, SOMEIP_EC_OK);

	sdHdr_set_flags(&sdHdrBasic, 1, 1, 1);

	sdEntry_set_type(&entryBasic, 0);
	sdEntry_set_idx1option(&entryBasic, 0);
	sdEntry_set_idx2option(&entryBasic, 0);
	sdEntry_set_numOfOptions(&entryBasic, 0, 0);
	sdEntry_set_serviceId(&entryBasic, 0);
	sdEntry_set_instanceId(&entryBasic, 0);
	sdEntry_set_majorVersion(&entryBasic, 0);
	sdEntry_set_ttl(&entryBasic, 0);
	sdEntry_set_minorVersion(&entryBasic, 0);

	sdOptionIpv4_set_length(&optionIpv4Basic, SD_LEN_IPV4);
	sdOptionIpv4_set_type(&optionIpv4Basic, 0);
	sdOptionIpv4_set_address(&optionIpv4Basic, 0);
	sdOptionIpv4_set_protocol(&optionIpv4Basic, SOMEIP_PROTOCOL);
	sdOptionIpv4_set_port(&optionIpv4Basic, 0);
}


/**
 * Initialization of Service Discovery service and basic SD structures. Construct
 * basic headers for find and offer service message.
 * @param ip4AddrLocalHost Pointer to local host IPv4 address
 * @param ip4AddrMulticast Pointer to multicast group IPv4 address
 * @return ERR_OK if everything was initialize properly otherwise err_t error code
 */
static err_t sdInit(ip4_addr_t *ip4AddrLocalHost, ip4_addr_t *ip4AddrMulticast)
{
	err_t status;
	u8_t *currentSdMsgPtr;

	status = initSdService(ip4AddrLocalHost, ip4AddrMulticast);
	if (status != ERR_OK) {
		return status;
	}

	initSdBasicStructures();

	gPbFindService = pbuf_alloc(PBUF_TRANSPORT, SOMEIP_SD_MIN_LENGTH_ALL, PBUF_POOL);
	currentSdMsgPtr = gPbFindService->payload;
	sdMessageInit(&currentSdMsgPtr, 1, 1);

	gPbOfferService = pbuf_alloc(PBUF_TRANSPORT, SOMEIP_SD_MIN_LENGTH_ALL, PBUF_POOL);
	currentSdMsgPtr = gPbOfferService->payload;
	sdMessageInit(&currentSdMsgPtr, 1, 1);

	return ERR_OK;
}


/**
 * Initialization of SOME/IP basic structures, Service Discovery and send IGMP
 * message for join to multicast group.
 * @param ip4AddrLocalHost Pointer to local host IPv4 address
 * @param ip4AddrMulticast Pointer to multicast group IPv4 address
 * @param cyclicFindOfferDelayS Value of delay between sended find or offer service messages in seconds
 * @return ERR_OK if everything was initialize properly otherwise err_t error code
 */
err_t someip_init(ip4_addr_t *ip4AddrLocalHost, ip4_addr_t *ip4AddrMulticast, u32_t cyclicFindOfferDelayS)
{
	err_t status;

	status = sdInit(ip4AddrLocalHost, ip4AddrMulticast);
	if (status != ERR_OK) {
		return status;
	}

#ifdef LWIP_RAND
	gInitialDelayMs = (LWIP_RAND() % (SD_INITIAL_DELAY_MAX_MS - SD_INITIAL_DELAY_MIN_MS)) + SD_INITIAL_DELAY_MIN_MS;
#else
	gInitialDelayMs = ((SD_INITIAL_DELAY_MAX_MS - SD_INITIAL_DELAY_MIN_MS) / 2) + SD_INITIAL_DELAY_MIN_MS;
#endif
	gInitialDelayDoneFlag = 0;
	gPreviousTimeMs = sys_now();
	gFindRepetitions = 0;
	gBasicFindDelayMs = cyclicFindOfferDelayS * SOMEIP_SEC_IN_MS;
	gCyclicFindDelayMs = gBasicFindDelayMs;
	gCyclicOfferDelayMs = cyclicFindOfferDelayS * SOMEIP_SEC_IN_MS;
	gCurrentFindDelayMs = 0;
	gCurrentOfferDelayMs = 0;

	gUserSdCallbackFcn = NULL;

	status = igmp_joingroup(ip4AddrLocalHost, ip4AddrMulticast);
	if (status != ERR_OK) {
		return status;
	}

	return ERR_OK;
}


/**
 * Function that trigger send find or offer service message if client or server
 * services were previously configured. More over it analyze TTL time of configured
 * methods. In case that TTL expired binded PCB is deleted.
 * @return ERR_OK if message was sent properly otherwise err_t error code
 */
err_t someip_task(void)
{
	u32_t baseDelayMs;  // [ms]
	u8_t clientIterationDoneFlag;
	struct someip_service_t *service;
	struct someip_method_t *method;
	struct someip_deviceInterface_t *prevDeviceInterface, *currDeviceInterface, *nextDeviceInterface;
	err_t status;

	baseDelayMs = sys_now() - gPreviousTimeMs;

	if (!gInitialDelayDoneFlag) {
		if (baseDelayMs <= gInitialDelayMs) {
			return ERR_OK;
		}
		else {
			gInitialDelayDoneFlag = 1;
			baseDelayMs = SOMEIP_SEC_IN_MS;
			gCurrentFindDelayMs = gCyclicFindDelayMs;
			gCurrentOfferDelayMs = gCyclicOfferDelayMs;
		}
	}

	if (baseDelayMs >= SOMEIP_SEC_IN_MS) {
		clientIterationDoneFlag = (clientServices != NULL) ? 0 : 1;
		for (service = (clientServices != NULL) ? clientServices : serverServices; service != NULL;) {
			for (method = service->methods; method != NULL; method = method->next) {
				prevDeviceInterface = NULL;
				currDeviceInterface = method->dstDevices;
				while(currDeviceInterface != NULL) {
					currDeviceInterface->ttl -= baseDelayMs;
					if (currDeviceInterface->ttl < 0) {
						nextDeviceInterface = currDeviceInterface->next;
						status = removeDeviceInterface(&(method->dstDevices), prevDeviceInterface, currDeviceInterface);
						currDeviceInterface = nextDeviceInterface;
					}
					else {
						prevDeviceInterface = currDeviceInterface;
						currDeviceInterface = currDeviceInterface->next;
					}
				}
			}
			if ((service->next == NULL) && (!clientIterationDoneFlag)) {
				clientIterationDoneFlag = 1;
				service = serverServices;
			}
			else {
				service = service->next;
			}
		}

		gCurrentFindDelayMs += baseDelayMs;
		if ((clientServices != NULL) && (gCurrentFindDelayMs >= gCyclicFindDelayMs) && (gFindRepetitions < SD_FIND_REPETITIONS_MAX)) {
			status = sendFindOfferService(gPbFindService);
			if (status != ERR_OK) {
				return status;
			}
			gCyclicFindDelayMs = (1 << gFindRepetitions) * gBasicFindDelayMs;
			++gFindRepetitions;
			gCurrentFindDelayMs = 0;
		}

		gCurrentOfferDelayMs += baseDelayMs;
		if ((serverServices != NULL) && (gCurrentOfferDelayMs >= gCyclicOfferDelayMs)) {
			status = sendFindOfferService(gPbOfferService);
			if (status != ERR_OK) {
				return status;
			}
			gCurrentOfferDelayMs = 0;
		}

		gPreviousTimeMs = sys_now();
	}

	return ERR_OK;
}


/**
 * Send SOME/IP format message from previously run client or server
 * @param firstService pointer to singly linked list contains previously defined services
 * @param serviceId ID of service that will be used in SOME/IP header
 * @param methodId ID of method that will be used in SOME/IP header
 * @param msgType Type of message defined in someip.h file as #define SOMEIP_MT_
 * @param pb Pointer to buffer with payload
 * @return ERR_OK if message was sent properly otherwise err_t error code
 */
static err_t someip_sendMsg(struct someip_service_t *firstService, u16_t serviceId, u16_t methodId, u8_t msgType, struct pbuf *pbOriginal)
{
	u8_t msgWasSendFlag;
	struct someip_service_t *service;
	struct someip_method_t *method;
	struct someip_deviceInterface_t *deviceInterface;
	struct pbuf *pb;
	struct someipHdr_t *someipHdr;
	err_t status;

	if (pbOriginal == NULL) {
		return ERR_ARG;
	}

	msgWasSendFlag = 0;
	for (service = firstService; service != NULL; service = service->next) {
		if (service->id == serviceId) {
			for (method = service->methods; method != NULL; method = method->next) {
				if (method->id == methodId) {
					for (deviceInterface = method->dstDevices; deviceInterface != NULL; deviceInterface = deviceInterface->next) {
						pb = pbuf_alloc(PBUF_TRANSPORT, SOMEIP_MIN_LENGTH_ALL + pbOriginal->len, PBUF_POOL);
						someipHdr = pb->payload;
						MEMCPY((u8_t *)someipHdr, (u8_t *)(&someipHdrBasic), SOMEIP_MIN_LENGTH_ALL);

						someipHdr_set_serviceId(someipHdr, serviceId);
						someipHdr_set_methodId(someipHdr, methodId);
						someipHdr_set_sessionId(someipHdr, deviceInterface->sessionId);
						someip_sessionIdInc(deviceInterface->sessionId);
						someipHdr_set_messageType(someipHdr, msgType);
						someipHdr_set_returnCode(someipHdr, SOMEIP_EC_OK);

						MEMCPY(((u8_t *)(pb->payload)) + SOMEIP_MIN_LENGTH_ALL, (u8_t *)(pbOriginal->payload), pbOriginal->len);

						someipUpdateLength(pb);
						status = udp_sendto(service->unicastSrcDevice->udpPcb, pb, &(deviceInterface->ip4Addr), deviceInterface->port);
						if (status != ERR_OK) {
							return status;
						}

						pbuf_free(pb);
						msgWasSendFlag = 1;
					}
				}
			}
		}
	}

	if (!msgWasSendFlag) {
		return ERR_ARG;
	}

	return ERR_OK;
}


/**
 * Send SOME/IP format message from previously run client
 * @param serviceId ID of service that will be used in SOME/IP header
 * @param methodId ID of method that will be used in SOME/IP header
 * @param msgType Type of message defined in someip.h file as #define SOMEIP_MT_
 * @param pb Pointer to buffer with payload
 * @return ERR_OK if message was sent properly otherwise err_t error code
 */
err_t someip_sendClientMsg(u16_t serviceId, u16_t methodId, u8_t msgType, struct pbuf *pb)
{
	return someip_sendMsg(clientServices, serviceId, methodId, msgType, pb);
}


/**
 * Send SOME/IP format message from previously run server
 * @param serviceId ID of service that will be used in SOME/IP header
 * @param methodId ID of method that will be used in SOME/IP header
 * @param msgType Type of message defined in someip.h file as #define SOMEIP_MT_
 * @param pb Pointer to buffer with payload
 * @return ERR_OK if message was sent properly otherwise err_t error code
 */
err_t someip_sendServerMsg(u16_t serviceId, u16_t methodId, u8_t msgType, struct pbuf *pb)
{
	return someip_sendMsg(serverServices, serviceId, methodId, msgType, pb);
}


/**
 * Add pointer to function that will be called during receive SD message instead
 * of automation handle Service Discovery
 * @param callbackFcn Pointer to function callback function
 * @return ERR_OK
 */
err_t someip_addCallbackForSdMsg(someipCallback_t callbackFcn)
{
	gUserSdCallbackFcn = callbackFcn;

	return ERR_OK;
}

/* END function definitions */

