/*
 * someip.h
 *
 *  Created on: 19.03.2021
 *      Author: Piotr Golyzniak
 */

#ifndef __SOMEIP_H_
#define __SOMEIP_H_

#include "lwip.h"


/* BEGIN typedef */

typedef void (*someipCallback_t)(struct pbuf *p, const ip_addr_t *addr, u16_t port, u16_t serviceId, u16_t methodId);

/* END typedef */


/* START define */

// SOME/IP

#define SOMEIP_PROTOCOL_VERSION		0x01
#define SOMEIP_INTERFACE_VERSION	0x01
#define SOMEIP_PROTOCOL_TCP			0x06
#define SOMEIP_PROTOCOL_UDP			0x11
#define SOMEIP_PROTOCOL 			SOMEIP_PROTOCOL_UDP
#define SOMEIP_MIN_LENGTH_ALL		16  // minimal length of all SOME/IP message [bytes]
#define SOMEIP_MSGID_LEN_LENGTH		8  // length of SOME/IP three fields: Service ID, Method ID and Length [bytes]
#define SOMEIP_MIN_LENGTH			8  // minimal value of Length field in SOME/IP message [bytes]
#define SOMEIP_METHOD_ID_MASK		0x7FFF
#define SOMEIP_SEC_IN_MS			1000

// message type
#define SOMEIP_MT_REQUEST 				0x00
#define SOMEIP_MT_REQUEST_NO_RETURN 	0x01
#define SOMEIP_MT_NOTIFICATION 			0x02
#define SOMEIP_MT_RESPONSE 				0x80
#define SOMEIP_MT_ERROR 				0x81
#define SOMEIP_MT_TP_REQUEST 			0x20
#define SOMEIP_MT_TP_REQUEST_NO_RETURN	0x21
#define SOMEIP_MT_TP_NOTIFICATION 		0x22
#define SOMEIP_MT_TP_RESPONSE 			0xa0
#define SOMEIP_MT_TP_ERROR 				0xa1

// error codes
#define SOMEIP_EC_OK 						0x00
#define SOMEIP_EC_NOT_OK 					0x01
#define SOMEIP_EC_UNKNOWN_SERVICE 			0x02
#define SOMEIP_EC_UNKNOWN_METHOD 			0x03
#define SOMEIP_EC_NOT_READY 				0x04
#define SOMEIP_EC_NOT_REACHABLE 			0x05
#define SOMEIP_EC_TIMEOUT 					0x06
#define SOMEIP_EC_WRONG_PROTOCOL_VERSION 	0x07
#define SOMEIP_EC_WRONG_INTERFACE_VERSION	0x08
#define SOMEIP_EC_MALFORMED_MESSAGE 		0x09
#define SOMEIP_EC_WRONG_MESSAGE_TYPE 		0x0a
#define SOMEIP_EC_E2E_REPEATED 				0x0b
#define SOMEIP_EC_E2E_WRONG_SEQUENCE 		0x0c
#define SOMEIP_EC_E2E 						0x0d
#define SOMEIP_EC_E2E_NOT_AVAILABLE 		0x0e
#define SOMEIP_EC_E2E_NO_NEW_DATA 			0x0f

// SOME/IP-SD

#define SOMEIP_SD_MIN_LENGTH_ALL	28  // minimal length of all SOME/IP-SD message [bytes]
#define SOMEIP_SD_MIN_LENGTH		20  // minimal value of Length field in SOME/IP-SD message [bytes]
#define SD_MIN_LENGTH				12  // minimal length of SD message (flags + 2xlengths) [bytes]
#define SD_SERVICE_ID	0xFFFF
#define SD_METHOD_ID 	0x8100
#define SD_PORT 		30490
#define	SD_OT_LEN_TYPE_SIZE		3
#define SD_TTL_INF		-1		// TTL infinity
#define SD_MAX_LENGTH	1400	// max length of SOME/IP-SD message

// SOME/IP-SD time
#define SD_INITIAL_DELAY_MIN_MS	100	 // [ms]
#define SD_INITIAL_DELAY_MAX_MS	500	 // [ms]
#define SD_FIND_REPETITIONS_MAX	3
#define SD_TTL_FIND_SERVICE		3 * SOMEIP_SEC_IN_MS
#define SD_TTL_OFFER_SERVICE	3 * SOMEIP_SEC_IN_MS
#define SD_TTL_SUBSCRIBE		300 * SOMEIP_SEC_IN_MS
#define SD_TTL_SUBSCRIBE_ACK	300 * SOMEIP_SEC_IN_MS

// SOME/IP-SD Entry Types
#define SD_ET_FIND_SERVICE	0x00
#define SD_ET_OFFER_SERVICE	0x01
#define SD_ET_SUBSCRIBE		0x06
#define SD_ET_SUBSCRIBE_ACK	0x07

// SOME/IP-SD Option Types
#define SD_OT_CONFIGURATION 	0x01
#define SD_OT_LOAD_BALANCING 	0x02
#define SD_OT_IPV4_ENDPOINT 	0x04
#define SD_OT_IPV6_ENDPOINT 	0x06
#define SD_OT_IPV4_MULTICAST 	0x14
#define SD_OT_IPV6_MULTICAST 	0x16
#define SD_OT_IPV4_SD_ENDPOINT 	0x24
#define SD_OT_IPV6_SD_ENDPOINT 	0x26

// SOME/IP-SD Option Lengths
#define SD_LEN_LOAD_BALANCING 	0x05
#define SD_LEN_IPV4			 	0x09
#define SD_LEN_IPV6			 	0x15

/* END define */


/* START enum */

enum someip_dir_t {SOMEIP_TRANSCEIVER, SOMEIP_RECEIVER};

/* END enum */


/* START structures */

/**
 * Structure for SOME/IP frame header
 */
PACK_STRUCT_BEGIN
struct someipHdr_t {
	PACK_STRUCT_FIELD(u16_t serviceId);
	PACK_STRUCT_FIELD(u16_t methodId);
	PACK_STRUCT_FIELD(u32_t length);
	PACK_STRUCT_FIELD(u16_t clientId);
	PACK_STRUCT_FIELD(u16_t sessionId);
	PACK_STRUCT_FIELD(u8_t protocolVersion);
	PACK_STRUCT_FIELD(u8_t interfaceVersion);
	PACK_STRUCT_FIELD(u8_t messageType);
	PACK_STRUCT_FIELD(u8_t returnCode);
} 	PACK_STRUCT_STRUCT;
PACK_STRUCT_END

// getters
#define someipHdr_serviceId(someipHdr) 			ntohs((someipHdr)->serviceId)
#define someipHdr_methodId(someipHdr) 			ntohs((someipHdr)->methodId)
#define someipHdr_length(someipHdr) 			ntohl((someipHdr)->length)
#define someipHdr_clientId(someipHdr) 			ntohs((someipHdr)->clientId)
#define someipHdr_sessionId(someipHdr) 			ntohs((someipHdr)->sessionId)
#define someipHdr_protocolVersion(someipHdr) 	((someipHdr)->protocolVersion)
#define someipHdr_interfaceVersion(someipHdr)	((someipHdr)->interfaceVersion)
#define someipHdr_messageType(someipHdr) 		((someipHdr)->messageType)
#define someipHdr_returnCode(someipHdr) 		((someipHdr)->returnCode)

// setters
#define someipHdr_set_serviceId(someipHdr, x) 			((someipHdr)->serviceId = htons(x))
#define someipHdr_set_methodId(someipHdr, x) 			((someipHdr)->methodId = htons(x))
#define someipHdr_set_length(someipHdr, x) 				((someipHdr)->length = htonl(x))
#define someipHdr_set_clientId(someipHdr, x) 			((someipHdr)->clientId = htons(x))
#define someipHdr_set_sessionId(someipHdr, x) 			((someipHdr)->sessionId = htons(x))
#define someipHdr_set_protocolVersion(someipHdr, x) 	((someipHdr)->protocolVersion = (x))
#define someipHdr_set_interfaceVersion(someipHdr, x)	((someipHdr)->interfaceVersion = (x))
#define someipHdr_set_messageType(someipHdr, x) 		((someipHdr)->messageType = (x))
#define someipHdr_set_returnCode(someipHdr, x) 			((someipHdr)->returnCode = (x))


/**
 * Structure for SD frame header
 */
PACK_STRUCT_BEGIN
struct sdHdr_t {
	PACK_STRUCT_FIELD(u8_t flags);
	PACK_STRUCT_FIELD(u8_t reserved1);
	PACK_STRUCT_FIELD(u16_t reserved2);
} 	PACK_STRUCT_STRUCT;
PACK_STRUCT_END

// getters
#define sdHdr_reebot(sdHdr) 			((u8_t) ((sdHdr)->flags >> 7))
#define sdHdr_unicast(sdHdr) 			((u8_t) (((sdHdr)->flags >> 6) & 0x1))
#define sdHdr_explicitInitData(sdHdr) 	((u8_t) (((sdHdr)->flags >> 5) & 0x1))

// setters
#define sdHdr_set_flags(sdHdr, reebot, unicast, explicitInitData)	\
	((sdHdr)->flags = ((reebot << 7) | ((unicast << 6) & 0x40) | ((explicitInitData << 5) & 0x20)))
#define sdHdr_set_reboot(sdHdr, reebot) ((sdHdr)->flags = ((reebot << 7) | ((sdHdr)->flags & 0x7F)))


/**
 * Structure for Entry in Entries Array in SD message
 */
PACK_STRUCT_BEGIN
struct sdEntry_t {
	PACK_STRUCT_FIELD(u8_t type);
	PACK_STRUCT_FIELD(u8_t idx1option);
	PACK_STRUCT_FIELD(u8_t idx2option);
	PACK_STRUCT_FIELD(u8_t numOfOptions);
	PACK_STRUCT_FIELD(u16_t serviceId);
	PACK_STRUCT_FIELD(u16_t instanceId);
	PACK_STRUCT_FIELD(u8_t majorVersion);
	PACK_STRUCT_FIELD(u8_t ttl1);
	PACK_STRUCT_FIELD(u16_t ttl2);
	PACK_STRUCT_FIELD(u8_t reserved);
	PACK_STRUCT_FIELD(u8_t flagCounter);
	PACK_STRUCT_FIELD(u16_t eventgroupId);
} 	PACK_STRUCT_STRUCT;
PACK_STRUCT_END

// getters
#define sdEntry_type(sdEntry) 				((sdEntry)->type)
#define sdEntry_idx1option(sdEntry) 		((sdEntry)->idx1option)
#define sdEntry_idx2option(sdEntry) 		((sdEntry)->idx2option)
#define sdEntry_numOfOptions1(sdEntry)		((u8_t) ((sdEntry)->numOfOptions >> 4))
#define sdEntry_numOfOptions2(sdEntry) 		((u8_t) (((sdEntry)->numOfOptions) & 0xF))
#define sdEntry_serviceId(sdEntry) 			ntohs((sdEntry)->serviceId)
#define sdEntry_instanceId(sdEntry) 		ntohs((sdEntry)->instanceId)
#define sdEntry_majorVersion(sdEntry) 		((sdEntry)->majorVersion)
#define sdEntry_ttl(sdEntry) 				\
	(((((sdEntry)->ttl1 << 16) & 0x00FF0000) | (ntohs((sdEntry)->ttl2) & 0x0000FFFF)) * SOMEIP_SEC_IN_MS)  // return ttl in [ms]
#define sdEntry_minorVersion(sdEntry) 		\
	((((sdEntry)->reserved << 24) & 0xFF000000) | (((sdEntry)->flagCounter << 16) & 0x00FF0000) | (ntohs((sdEntry)->eventgroupId) & 0x0000FFFF))
#define sdEntry_initDataReqFlag(sdEntry) 	((sdEntry)->flagCounter >> 7)
#define sdEntry_counter(sdEntry) 			((sdEntry)->flagCounter & 0xF)
#define sdEntry_eventgroupId(sdEntry) 		ntohs((sdEntry)->eventgroupId)

// setters
#define sdEntry_set_type(sdEntry, x) 			((sdEntry)->type = (x))
#define sdEntry_set_idx1option(sdEntry, x) 		((sdEntry)->idx1option = (x))
#define sdEntry_set_idx2option(sdEntry, x) 		((sdEntry)->idx2option = (x))
#define sdEntry_set_numOfOptions(sdEntry, opt1, opt2) \
	((sdEntry)->numOfOptions = ((opt1) << 4) | ((opt2) & 0xF))
#define sdEntry_set_serviceId(sdEntry, x) 		((sdEntry)->serviceId = htons(x))
#define sdEntry_set_instanceId(sdEntry, x) 		((sdEntry)->instanceId = htons(x))
#define sdEntry_set_majorVersion(sdEntry, x) 	((sdEntry)->majorVersion = (x))
#define sdEntry_set_ttl(sdEntry, x) 			\
	do { \
		(sdEntry)->ttl1 = ((u8_t) ((htonl((u32_t)(x / SOMEIP_SEC_IN_MS)) >> 8) & 0xFF)); \
		(sdEntry)->ttl2 = ((u16_t) ((htonl((u32_t)(x / SOMEIP_SEC_IN_MS)) >> 16) & 0xFFFF)); \
	} while(0)  // x is in [ms], but set field in [s]
#define sdEntry_set_minorVersion(sdEntry, x) 	\
	do { \
		(sdEntry)->reserved = ((u8_t) (htonl(x) & 0xFF)); \
		(sdEntry)->flagCounter = ((u8_t) ((htonl(x) >> 8) & 0xFF)); \
		(sdEntry)->eventgroupId = ((u16_t) ((htonl(x) >> 16) & 0xFFFF)); \
	} while(0)
#define sdEntry_set_flagCounter(sdEntry, flag, counter) \
	((sdEntry)->flagCounter = ((flag << 7) | (counter & 0xF)))
#define sdEntry_set_eventgroupId(sdEntry, x) 	((sdEntry)->eventgroupId = htons(x))


/**
 * Structure for IPv4 option in Options Array in SD message
 */
PACK_STRUCT_BEGIN
struct sdOptionIpv4_t {
	PACK_STRUCT_FIELD(u16_t length);
	PACK_STRUCT_FIELD(u8_t type);
	PACK_STRUCT_FIELD(u8_t reserved1);
	PACK_STRUCT_FIELD(u32_t address);
	PACK_STRUCT_FIELD(u8_t reserved2);
	PACK_STRUCT_FIELD(u8_t protocol);
	PACK_STRUCT_FIELD(u16_t port);
} 	PACK_STRUCT_STRUCT;
PACK_STRUCT_END

// getters
#define sdOptionIpv4_length(sdOptionIpv4) 	ntohs((sdOptionIpv4)->length)
#define sdOptionIpv4_type(sdOptionIpv4) 	((sdOptionIpv4)->type)
#define sdOptionIpv4_address(sdOptionIpv4) 	((sdOptionIpv4)->address)  // do not change order - IPv4 addresses in lwip are in network order
#define sdOptionIpv4_protocol(sdOptionIpv4) ((sdOptionIpv4)->protocol)
#define sdOptionIpv4_port(sdOptionIpv4) 	ntohs((sdOptionIpv4)->port)

// setters
#define sdOptionIpv4_set_length(sdOptionIpv4, x) 	((sdOptionIpv4)->length = htons(x))
#define sdOptionIpv4_set_type(sdOptionIpv4, x) 		((sdOptionIpv4)->type = (x))
#define sdOptionIpv4_set_address(sdOptionIpv4, x) 	((sdOptionIpv4)->address = (x))  // x should be 32bit variable with IPv4 network order
#define sdOptionIpv4_set_protocol(sdOptionIpv4, x) 	((sdOptionIpv4)->protocol = (x))
#define sdOptionIpv4_set_port(sdOptionIpv4, x) 		((sdOptionIpv4)->port = htons(x))


/**
 * Structure for IPv6 option in Options Array in SD message
 */
PACK_STRUCT_BEGIN
struct sdOptionIpv6_t {
	PACK_STRUCT_FIELD(u16_t length);
	PACK_STRUCT_FIELD(u8_t type);
	PACK_STRUCT_FIELD(u8_t reserved1);
	PACK_STRUCT_FIELD(u32_t address1);
	PACK_STRUCT_FIELD(u32_t address2);
	PACK_STRUCT_FIELD(u32_t address3);
	PACK_STRUCT_FIELD(u32_t address4);
	PACK_STRUCT_FIELD(u8_t reserved2);
	PACK_STRUCT_FIELD(u8_t protocol);
	PACK_STRUCT_FIELD(u16_t port);
} 	PACK_STRUCT_STRUCT;
PACK_STRUCT_END

// getters
#define sdOptionIpv6_length(sdOptionIpv6) 	ntohs((sdOptionIpv6)->length)
#define sdOptionIpv6_type(sdOptionIpv6) 	((sdOptionIpv6)->type)
#define sdOptionIpv6_address1(sdOptionIpv6) ntohl((sdOptionIpv6)->address1)
#define sdOptionIpv6_address2(sdOptionIpv6) ntohl((sdOptionIpv6)->address2)
#define sdOptionIpv6_address3(sdOptionIpv6) ntohl((sdOptionIpv6)->address3)
#define sdOptionIpv6_address4(sdOptionIpv6) ntohl((sdOptionIpv6)->address4)
#define sdOptionIpv6_protocol(sdOptionIpv6) ((sdOptionIpv6)->protocol)
#define sdOptionIpv6_port(sdOptionIpv6) 	ntohs((sdOptionIpv6)->port)

// setters
#define sdOptionIpv6_set_length(sdOptionIpv6, x) 	((sdOptionIpv6)->length = htons(x))
#define sdOptionIpv6_set_type(sdOptionIpv6, x) 		((sdOptionIpv6)->type = (x))
#define sdOptionIpv6_set_address1(sdOptionIpv6, x) 	((sdOptionIpv6)->address1 = htonl(x))
#define sdOptionIpv6_set_address2(sdOptionIpv6, x) 	((sdOptionIpv6)->address2 = htonl(x))
#define sdOptionIpv6_set_address3(sdOptionIpv6, x) 	((sdOptionIpv6)->address3 = htonl(x))
#define sdOptionIpv6_set_address4(sdOptionIpv6, x) 	((sdOptionIpv6)->address4 = htonl(x))
#define sdOptionIpv6_set_protocol(sdOptionIpv6, x) 	((sdOptionIpv6)->protocol = (x))
#define sdOptionIpv6_set_port(sdOptionIpv6, x) 		((sdOptionIpv6)->port = htons(x))


/**
 * Structure for Configuration option in Options Array in SD message
 */
PACK_STRUCT_BEGIN
struct sdOptionConf_t {
	PACK_STRUCT_FIELD(u16_t length);
	PACK_STRUCT_FIELD(u8_t type);
	PACK_STRUCT_FIELD(u8_t reserved1);
} 	PACK_STRUCT_STRUCT;
PACK_STRUCT_END

// getters
#define sdOptionConf_length(sdOptionConf) 	ntohs((sdOptionConf)->length)
#define sdOptionConf_type(sdOptionConf) 	((sdOptionConf)->type)
#define sdOptionConf_dataPtr(sdOptionConf) 	((u8_t *) (sdOptionConf + 1))

// setters
#define sdOptionConf_set_length(sdOptionConf, x) 	((sdOptionConf)->length = htons(x))
#define sdOptionConf_set_type(sdOptionConf, x) 		((sdOptionConf)->type = (x))


/**
 * Structure for Load balancing option in Options Array in SD message
 */
PACK_STRUCT_BEGIN
struct sdOptionLoad_t {
	PACK_STRUCT_FIELD(u16_t length);
	PACK_STRUCT_FIELD(u8_t type);
	PACK_STRUCT_FIELD(u8_t reserved1);
	PACK_STRUCT_FIELD(u16_t priority);
	PACK_STRUCT_FIELD(u16_t weight);
} 	PACK_STRUCT_STRUCT;
PACK_STRUCT_END

// getters
#define sdOptionLoad_length(sdOptionLoad) 	ntohs((sdOptionLoad)->length)
#define sdOptionLoad_type(sdOptionLoad) 	((sdOptionLoad)->type)
#define sdOptionLoad_priority(sdOptionLoad) ntohs((sdOptionLoad)->priority)
#define sdOptionLoad_weight(sdOptionLoad) 	ntohs((sdOptionLoad)->weight)

// setters
#define sdOptionLoad_set_length(sdOptionLoad, x) 	((sdOptionLoad)->length = htons(x))
#define sdOptionLoad_set_type(sdOptionLoad, x) 		((sdOptionLoad)->type = (x))
#define sdOptionLoad_set_priority(sdOptionLoad, x) 	((sdOptionLoad)->priority = htons(x))
#define sdOptionLoad_set_weight(sdOptionLoad, x) 	((sdOptionLoad)->weight = htons(x))


/**
 * Macros help to get or set specified SOME/IP frame fields
 */
#define someip_sessionIdInc(id) ((((id) + 1) != 0) ? (++id) : (id=1))
#define someip_set_sessionId(payloadPtr, x) (*((u16_t *) (payloadPtr + 10)) = htons(x))
#define sdHdr_arrayLength(payloadPtr) ntohl(*((u32_t *) (payloadPtr)))
#define sdHdr_set_arrayLength(payloadPtr, x) (*((u32_t *) (payloadPtr)) = htonl(x))
#define sdHdr_optionLength(payloadPtr) ntohs(*((u16_t *) (payloadPtr)))
#define sdHdr_optionType(payloadPtr) (*((u8_t *) (payloadPtr + 2)))


/**
 * Structure for device interface contains information about IP address,
 * port number, session ID, binded PCB and TTL
 */
struct someip_deviceInterface_t {
	struct someip_deviceInterface_t *next;
	s32_t ttl;  // [ms]
	struct ip4_addr ip4Addr;
	u16_t port;
	u8_t protocol;  // UDP or TCP
	u16_t sessionId;
	struct udp_pcb *udpPcb;
};


/**
 * Structure contains information about method in SOME/IP protocol - ID number,
 * destination device interface, callback function trigger during receive message
 * with special service and metod ID
 */
struct someip_method_t {
	struct someip_method_t *next;
	u16_t id;
	struct someip_deviceInterface_t *dstDevices;
	someipCallback_t callbackFcn;
	u32_t flagToSubscribeAckAndTtl;
};


/**
 * Structure contains information about service in SOME/IP protocol - ID number,
 * assigned methods, and device interface
 */
struct someip_service_t {
	struct someip_service_t *next;
	u16_t id;
	struct someip_method_t *methods;
	struct someip_deviceInterface_t *unicastSrcDevice;
	struct someip_deviceInterface_t *multicastSrcDevice;
	u8_t flagToSubscribe;
};

/* END structures */


/* START user function declaration */

err_t someip_init(ip4_addr_t *ip4AddrLocalHost, ip4_addr_t *ip4AddrMulticast,
		u32_t cyclicFindOfferDelayS);
err_t someip_addClientService(struct someip_service_t **newService, u16_t serviceId,
		ip4_addr_t *serviceIpv4Addr, u16_t servicePort, u8_t serviceProtocol);
err_t someip_addServerService(struct someip_service_t **newService, u16_t serviceId,
		ip4_addr_t *serviceIpv4Addr, u16_t servicePort, u8_t serviceProtocol);
err_t someip_removeClientService(u16_t serviceId);
err_t someip_removeServerService(u16_t serviceId);
err_t someip_addMethod(struct someip_service_t *service, u16_t methodId,
		someipCallback_t callbackFcn);
err_t someip_removeMethod(struct someip_service_t *service, u16_t methodId);
err_t someip_addCallbackForSdMsg(someipCallback_t callbackFcn);
err_t someip_sendClientMsg(u16_t serviceId, u16_t methodId, u8_t msgType,
		struct pbuf *pb);
err_t someip_sendServerMsg(u16_t serviceId, u16_t methodId, u8_t msgType,
		struct pbuf *pb);
err_t someip_task(void);

/* END user function declaration */

#endif  /* __SOMEIP_H_ */
